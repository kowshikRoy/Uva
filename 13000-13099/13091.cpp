#include <bits/stdc++.h>
using namespace std;
char a[5][5];
int main()
{
    int t,col,x,y,cs=0;
    cin >> t;
    while(t--) {
        for(int i=0; i < 5;i ++ )cin >> a[i];
        col = -1;
        for(int i = 0; i < 5;i ++ ) {
            for(int j = 0 ; j < 5; j ++ ) {
                if(a[i][j] == '|') col = j;
                if(a[i][j] == '<' || a[i][j]=='>') {
                    x = i;
                    y = j;
                }
            }
        }
        bool flag = 1;
        if(a[x][y]=='>') {
            if(y>col) flag = 0;
        }
        if(a[x][y] == '<') {
            if(y < col) flag = 0;
        }
        printf("Case %d: %s Ball\n",++cs, flag ?"Thik" :"No");
    }
}