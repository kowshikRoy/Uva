#include <bits/stdc++.h>
using namespace std;
#define MP	make_pair
const int MOD = 10007;
const int N = 1001;
char s[255];
int d;
int CS=1;
int vis[255][255];
pair<int,int> dp[255][255];

bool isMatch(int i,int j)
{
	if(j-i>d) {
		if((s[i]=='A' &&s[j]=='U') || (s[i]=='U' && s[j]=='A') ||
			(s[i]=='C' && s[j]=='G') || (s[i]=='G' && s[j]=='C')) return 1;
	}
	return 0;
}
pair<int,int> solve(int i,int j)
{
	if(i>=j) return MP(0,1);
	if(vis[i][j] == CS) return dp[i][j];
	pair<int,int>res = MP(-1,1);
	for(int k = i + d; k <= j; k ++ ) {
		if(isMatch(i,k)) {
			auto b = solve(i+1,k-1);
			auto c = solve(k+1,j);
			b = MP(b.first + c.first + 1, b.second * c.second % MOD);
			if(b.first > res.first) {
				res = b;
			}
			else if(b.first == res.first ){
				res.second += b.second;
			}
			res.second %= MOD;
		} 
	}
	pair<int,int> res2 = solve(i+1,j);
	if(res2.first > res.first ) {
		res = res2;
	}
	else if(res2.first == res.first) res.second += res2.second;
	res.second %= MOD;
	vis[i][j] = CS;
	return  dp[i][j] = res;
}

int main()
{
	//freopen("/Users/MyMac/Desktop/in.txt","r",stdin);
	int t;
	scanf("%d",&t);
	while(t--) {
		scanf("%d",&d);
		scanf("%s",s);
		
		auto ans = solve(0,strlen(s)-1);
		printf("Case %d: %d %d\n", CS ++ , ans.first, ans.second );
	}
	return 0;
}