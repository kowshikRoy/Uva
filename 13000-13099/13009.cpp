#include<bits/stdc++.h>
using namespace std;
const int N = 1e5+5;
int p,v;
struct PT{
    int x,y,id;
    bool operator<(const PT &p ) const {return x<p.x;}
}pt[N], vt[N];
vector<int> in;
int b[2*N];

map<int,vector<int> > ST, END;

void update(int x,int v)
{
    while(x<2*N) b[x] +=v, x+=x&-x;
}
int query(int x)
{
    int res = 0;
    while(x>0) res+= b[x], x -=x&-x;
    return res;
}
int main()
{
    while(scanf("%d %d",&p,&v)==2){
        for(int i = 0; i < p; i ++) {
            scanf("%d %d",&pt[i].x, &pt[i].y);
            pt[i].id = i+ 1;
            in.push_back(pt[i].y);
        }
        sort(pt,pt+p);
        for(int i = 0;i < v;i ++ ) {
            scanf("%d %d",&vt[i].x, &vt[i].y);
            in.push_back(vt[i].y);
        }
        for(int i = 0; i < v ; i ++ ) {
            PT nxt = vt[(i+1)%v];
            if(nxt.y == vt[i].y){
                int L = min(vt[i].x , nxt.x);
                int R = max(vt[i].x , nxt.x);
                ST[L].push_back (vt[i].y);
                END[R].push_back(vt[i].y);
            }
        }
        map<int ,vector<int > >::iterator it = ST.begin() , it2 = END.begin();
        memset(b,0,sizeof b);
        sort(in.begin(), in.end());
        in.resize(unique(in.begin(), in.end()) - in.begin());
        
        long long ans = 0;
        for(int i = 0; i < p; i ++ ) {
            while(it != ST.end()){
                int x = it->first;
                if(x <= pt[i].x){
                    for(auto a: ST[x]) {
                        int pos=  lower_bound(in.begin(), in.end(), a) - in.begin() + 1;
                        update(pos,1);
                    }
                    it ++;
                }
                else break;
            }
            while(it2 != END.end()){
                int x= it2->first;
                if( x <= pt[i].x) {
                    for(auto a: END[x]) {
                        int pos=  lower_bound(in.begin(), in.end(), a) - in.begin() + 1;
                        update(pos,-1);
                    }
                    it2 ++;
                }
                else break;
            }
            int y = pt[i].y;
            int pos =lower_bound(in.begin(), in.end(), y) - in.begin() + 1;
            int k = query(pos);
            if(k % 2 == 0) {
                ans += pt[i].id ;
            }
        }
        ST.clear();
        END.clear();
        printf("%lld\n",ans);
        in.clear();
    }
}