#include <bits/stdc++.h>
using namespace std;
const int N = 2e5+5;
int pos[N], a[N];
int main()
{
    int t,n,p,cs=0;
    scanf("%d",&t);
    while(t--) {
        scanf("%d",&n);
        for(int i =0 ;i < n; i ++ ) {
            scanf("%d",&p);
            pos[p] = i;
        }
        for(int i = 0;i < n; i ++) {
            scanf("%d",&p);
            a[i] = pos[p];
        }
        vector<int> ar;
        int ans = 0;
        for(int i = 0; i < n ;i ++ ) {
            int k = upper_bound(ar.begin(), ar.end(),a[i]) - ar.begin();
            if(k==ar.size()) ar.push_back(a[i]);
            else ar[k] = a[i];
            ans = max(ans ,(int) ar.size());
        }
        printf("Case %d: %d\n",++cs, 2*n-2*ans );
    }
}
/*
2
5
1 3 5 4 2
1 5 4 3 2
4
1 2 4 3
3 4 2 1
*/