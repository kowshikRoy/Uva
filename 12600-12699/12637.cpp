#include <bits/stdc++.h>
using namespace std;
int INF = 1e8;
const int N = 255;
struct Hungarian {
    
    int n ;                 // Number of Row
    int m ;                 // Number of column
    int visx[N];            // Status of X( Left )
    int visy[N];            // Status of Y ( Right)
    int fa[N];              // Father / Matched Vertex of i(Right)
    int dis[N][N];          // Given Matrix ( I need to Maximize )
    int slack[N];           // Very Important , Among all the vertex incident to y
    int lx[N];              // Vertex label of X side
    int ly[N];              // Vertex labeling of Y side
    int lack;
    
    bool dfs( int i )
    {
        visx[i] = 1;
        for( int j = 0 ; j < n ; j ++ ) {
            if( visy[j] ) continue;
            int t = lx[i] + ly[j] - dis[i][j];
            if( t == 0 ){
                visy[j] = 1;
                if( fa[j] == -1 || dfs(fa[j]) ) {
                    fa[j] = i ;
                    return 1;
                }
            }
            else {
                lack = min( lack , t) ;
            }
        }
        return 0;
    }
    
    
    int km()
    {
        memset(ly, 0, sizeof(ly));
        for(int i = 0; i < n ; i ++ ) {
            lx[i] = INT_MIN;
            for( int j =0 ; j < n ; j ++ ) {
                lx[i] = max( lx[i] , dis[i][j]);
            }
        }
        
        memset(fa, -1, sizeof(fa));
        for ( int i = 0; i < n ; i ++ ){
            while (1) {
                memset(visx, 0, sizeof(visx));
                memset(visy, 0, sizeof(visy));
                lack = INF;
                if( dfs(i) ) break;     // Found A Matching for i
                
                for( int j = 0; j <n ; j ++ ) {
                    if(visx[j] ) lx[j]-= lack;
                }
                for( int j = 0 ; j < n ; j ++ ) {
                    if( visy[j] ) ly[j] += lack;
                }
            }
        }
        
        int ans = 0;
        for( int i = 0; i < n ; i ++ ){
            if( fa[i] == -1 || dis[fa[i]][i] == - INF ) continue;
            ans += dis[fa[i]][i];
        }
        return ans;
        
    }
    
}h;

int px[N],py[N],cx[N],cy[N];
int dist(int i,int j)
{
    return abs(px[i]-cx[j]) + abs(py[i] - cy[j]);
}
int main()
{
    //freopen("/Users/MyMac/Desktop/in.txt","r",stdin);
    int n,m;
    while(scanf("%d %d",&n,&m)==2) {
        for(int i = 1; i <= n; i ++) {
            scanf("%d %d",&px[i],&py[i]);
        }
        for(int i = 1; i <= m ; i++ ) {
            scanf("%d %d",&cx[i],&cy[i]);
        }
        memset(h.dis,0,sizeof h.dis);
        h.n=n;
        for(int i = 1; i <=  n; i ++ ) {
            for(int j = 1; j <= m ; j ++ ) {
                int d = dist(i,j);
                h.dis[i-1][j-1] = -d;
            }
            
        }
        
        printf("%d\n", -h.km());
    }
    return 0;
}

