#include<bits/stdc++.h>
using namespace std;

const int N = 1001;
char voc[505][33];
char ch[505][33];

int n,m;
bool vis[505];
int match[505];
std::vector<int> G[505];
int cnt1[505][26],cnt2[505][26];

bool dfs(int u)
{
	if(vis[u]) return 0;
	vis[u] = 1;
	for(int i = 0; i< G[u].size(); i ++ ) {
		int v = G[u][i];
		if(match[v] == -1 || dfs(match[v])) {
			match[v] = u;
			return 1;
		}
	}
	return 0;
}
bool can(int i,int j)
{
	
	for(int let = 0; let < 26; let++ ) {
		if(cnt2[j][let] > cnt1[i][let]) return 0;
	}
	return 1;
}
int main()
{
	//freopen("/Users/MyMac/Desktop/in.txt","r",stdin);
	while(scanf("%d %d",&n,&m)==2) {
		for(int i = 0 ;i < n; i ++ ) {
			scanf("%s",voc[i]);
			for(int j = 0; voc[i][j]; j ++ ) cnt1[i][voc[i][j]-'a'] ++;
		}
 		for(int i = 0; i < m ; i ++ ) {
			scanf("%s",ch[i]);
			for(int j = 0; ch[i][j]; j ++ ) cnt2[i][ch[i][j]-'a'] ++;
		}
		for(int i = 0; i < n ;i ++ ) {
			for(int j = 0; j < m ; j ++ ) {
				if(can(i,j)) G[i].push_back(j);
			}
		}
		memset(match,-1,sizeof match);
		int res = 0;
		for(int i= 0; i < n ; i++ ) {
			memset(vis,0,sizeof vis);
			if(dfs(i)) res ++;
		}
		printf("%d\n", res);
		for(int i=0;i < n;i++ ) G[i].clear();
		memset(cnt1,0,sizeof cnt1);
		memset(cnt2,0,sizeof cnt2);
	}
	return 0;
}