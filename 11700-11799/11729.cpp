#include <bits/stdc++.h>
using namespace std;
pair<int,int> p[1005];
bool cmp(pair<int,int> a, pair<int,int> b)
{
    return a.second > b.second;
}
int main()
{
    int n,cs=0;
    while(scanf("%d",&n)==1 && n) {
        for(int i=0 ;i<n ;i ++){
            scanf("%d %d",&p[i].first, &p[i].second);
        }
        sort(p,p+n,cmp);
        int ans = 0, bf = 0;
        for(int i =0;i < n; i ++ ) {
            bf += p[i].first;
            ans = max(ans , bf + p[i].second);
            
        }
        printf("Case %d: %d\n",++cs, ans);
    }
}
