//
//  main.cpp
//  103 - Stacking Boxes
//
//  Created by MAC on 3/20/14.
//  Copyright (c) 2014 MAC. All rights reserved.
//

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<map>
#include<queue>
#include<stack>
#include<cstdlib>
#include<algorithm>
#include<set>
#include<iterator>
#include<cassert>

using namespace std;

/*------- Constants---- */
#define MX 1030
#define ll long long
#define ull unsigned long long
#define mod 1000000007

/* -------Global Variables ------ */
ll x,y;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))


/*------ template functions ------ */
template < class T > inline T gcd(T a , T b ) { if(b==0) return a; else return gcd(b, a%b);}
template < class T > inline T lcm(T a , T b ) { return  a*b / gcd(a, b);}
template < class T > inline T extended_euclid_returning_gcd(T a,T b){ T t,d; if(b==0){d = a;x=1;y=0;} else {extended_euclid_returning_gcd(b, a%b); t=x; x=y;y = t-(a*y/b);}}
template < class T > inline T absolute(T a ) { if(a>0) return a; else return -a;}
template < class T > inline T reverse_num( T a ){ T x=0,tmp = a; while(tmp) { x=10*x+ tmp % 10; tmp/=10;} return x;}
template <class T > inline T big_mod(T base, T power){ T res=1; while (power) { if(power&1){ res*=base; power--;} base =base *base; power/=2;} return res;}

int dim[35][15],dp[35],n;
vector<int> a[35],uku;

bool toor(int i,int m)
{
    for (int j=1; j<n; j++) {
        if(dim[i][j]>= dim[m][j]) return false;
    }
    return true;
}
int calc(int po)
{
    int i,ans=1,tmp;
    for (i=1; i<po; i++) {
        if(toor(i,po)){
            tmp= 1+dp[i];
            if(tmp>ans) {ans=tmp; uku = a[i];}
        }
    }
    a[po] =  uku;
    a[po].push_back(po);
    uku.clear();
    return dp[po]=ans;
}

int main()
{
    int k,i,maxi,j,ans,track;
    while (scanf("%d %d",&k,&n)==2) {
        for (i=1; i<=k; i++) {
            for (j=0; j<n; j++) {
                scanf("%d",&dim[i][j]);
            }
            sort(dim[i], dim[i]+n);
        }
        maxi=-1;
        for (i=1; i<=k; i++) {
            ans = calc(i);
            if(ans>maxi) {
                maxi=ans;
                track=i;
            }
        }
        printf("%d\n",maxi);
        for(i=1;i<a[track].size();i++) printf("%d ",a[track][i]);
    }
    return 0;
}