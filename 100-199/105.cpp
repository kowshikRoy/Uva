//
//  main.cpp
//  105 - The Skyline Problem
//
//  Created by Repon Macbook on 4/5/15.
//  Copyright (c) 2015 Repon Macbook. All rights reserved.
//

#include <bits/stdc++.h>
using namespace std;

/*------- Constants---- */
#define LMT                     100005
#define ll                      long long
#define ull                     unsigned long long
#define mod                     1000000007
#define MEMSET_INF              63
#define MEM_VAL                 1061109567
#define FOR(i,n)                for( int i=0 ; i < n ; i++ )
#define mp(i,j)                 make_pair(i,j)
#define lop(i,a,b)              for( int i = (a) ; i < (b) ; i++)
#define pb(a)                   push_back((a))
#define gc                      getchar_unlocked
#define PI                      acos(-1.0)
#define inf                     1<<30
#define lc                      ((n)<<1)
#define rc                      ((n)<<1 |1)
#define msg(x)			  cout<<x<<endl;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;
/*------ template functions ------ */
inline void sc(int &x)
{
	register int c = gc();
	x = 0;
	int neg = 0;
	for(;((c<48 | c>57) && c != '-');c = gc());
	if(c=='-') {neg=1;c=gc();}
	for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
	if(neg) x=-x;
}

template <class T> inline T bigmod(T p,T e,T M){
	ll ret = 1;
	for(; e > 0; e >>= 1){
		if(e & 1) ret = (ret * p) % M;
		p = (p * p) % M;
	} return (T)ret;
}
template <class T> inline T gcd(T a,T b){if(b==0)return a;return gcd(b,a%b);}
template <class T> inline T modinverse(T a,T M){return bigmod(a,M-2,M);}

/*************************** END OF TEMPLATE ****************************/

int dp[LMT];
int main()
{
	
	int a, b , l , iM  = - 1;
	while (scanf("%d %d %d",&a,&l,&b) == 3 ) {
		//if(a==0) break;
		if(a>b) swap(a,b);
		for (int i = a ; i < b && l ; i ++ ) {
			if(l > dp[i]) dp[i] = l;
		}
		if(b > iM && l ) iM = b ;
	}
	int i ;
	dp[iM + 1] = -1;
	int end = 0;
	for( i = 0; i <= iM ; i++ ) if( dp[i]) break;
	printf("%d ", i);
	for (int j = i + 1  ; j <= iM  ;  ) {
		if( dp[j] == dp[i] ) {
			end = j;
			j ++;
		}
		else {
			printf("%d %d " , dp[i] ,end + 1 );
			i = end + 1;
		}
	}
	printf("0\n");
	return 0;
}

/*
 1 11 5
 2 6 7
 3 13 9 
 12 7 16 
 14 3 25 
 19 18 22 
 23 13 29
 24 4 28
 */
