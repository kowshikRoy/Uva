//
//  main.cpp
//  100 - The 3n + 1 problem
//
//  Created by MAC on 2/9/14.
//  Copyright (c) 2014 MAC. All rights reserved.
//

#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;
#define N 1000001
#define ll long long
ll ara[N],m;

ll foo(ll n)
{
    ll p=0;
    while (n!=1) {
        p++;
        if(n&1)n=3*n+1;
        else n>>=1;
        if(n<N && ara[n]>0) return ara[n]+p;
    }
    return p;
}
int main(int argc, const char * argv[])
{


    ll a,b,max=-1;
    ara[1]=1;
    for (ll i=2; i<N; i++) {
        if(i&1) ara[i]=foo(i);
        else ara[i]=1+ara[i>>1];
    }
    while (scanf("%lld%lld",&a,&b)==2) {
        ll l=a,h=b;
        if(l>h)swap(l, h);
        max=-1;
        for (ll i=l; i<=h; i++) {
            if(ara[i]>max)max=ara[i];
        }
        printf("%lld %lld %lld\n",a,b,max);
    }
    return 0;
}
