//
//  main.cpp
//  108 - Maximum Sum
//
//  Created by Repon Macbook on 7/5/15.
//  Copyright (c) 2015 MAC. All rights reserved.
//
#include <bits/stdc++.h>
using namespace std;

/*------- Constants---- */
#define LMT				105
#define ll				long long
#define ull				unsigned long long
#define mod				1000000007
#define MEMSET_INF		63
#define MEM_VAL			1061109567
#define FOR(i,n)			for( int i=0 ; i < n ; i++ )
#define mp(i,j)			make_pair(i,j)
#define lop(i,a,b)		for( int i = (a) ; i < (b) ; i++)
#define pb(a)			push_back((a))
#define gc				getchar
#define PI				acos(-1.0)
#define inf				1<<30
#define lc				((n)<<1)
#define rc				((n)<<1 |1)
#define debug(x)              cout <<#x <<" -> " << x << endl;
#define EPS				1e-7
#define fred()                 freopen("in.txt","r",stdin);
#define fwrt()               freopen("in.txt","w",stdout);
/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;
/*------ template functions ------ */
inline void sc(int &x)
{
	register int c = gc();
	x = 0;
	int neg = 0;
	for(;((c<48 | c>57) && c != '-');c = gc());
	if(c=='-') {neg=1;c=gc();}
	for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
	if(neg) x=-x;
}

template <class T> inline T bigmod(T p,T e,T M){
	ll ret = 1;
	for(; e > 0; e >>= 1){
		if(e & 1) ret = (ret * p) % M;
		p = (p * p) % M;
	} return (T)ret;
}
template <class T> inline T gcd(T a,T b){if(b==0)return a;return gcd(b,a%b);}
template <class T> inline T modinverse(T a,T M){return bigmod(a,M-2,M);}
template <class T > inline void extEuclid(T  a, T b, T &x, T &y, T &gcd) {
      x = 0; y = 1; gcd = b;
      T m, n, q, r;
      for (T u=1, v=0; a != 0; gcd=a, a=r) {
            q = gcd / a; r = gcd % a;
            m = x-u*q; n = y-v*q;
            x=u; y=v; u=m; v=n;
      }
}

// The result could be negative, if it's required to be positive, then add "m"
template <class T > inline T  modInv(T n, T m) {
      T x, y, gcd;
      extEuclid(n, m, x, y, gcd);
      if (gcd == 1) return x % m;
      return 0;
}

/*************************** END OF TEMPLATE ****************************/

int iAr[LMT][LMT];
int T[LMT];


int kadane( int n)
{
      int sum = 0  , mx = 0  ;
      for(int i = 0 ;i < n ; i ++ ) {
            sum += T[i];
            if( sum < 0 ) {
                  sum = 0;
            }
            if( sum > mx ) {
                  mx = sum;
            }
      }
      if( mx == 0 ) {
            mx = -inf;
            for(int i = 0; i < n ; i ++ ) mx= max( mx , T[i]) ;
      }
      return mx;
}
int calc( int row , int col )
{
      
      int iM = -inf;
      for( int i = 0; i < row ; i ++ ) {
            ms(T, 0);
            for (int j = i ; j < row ; j ++ ) {
                  for(int k = 0; k < col ; k ++) T[k] += iAr[j][k];
                  int p = kadane(col );
                  if( p > iM ) iM = p;
            }
      }
      return iM;
}
int main()
{
      int n;
      while (scanf("%d",&n) == 1) {
            for(int i = 0 ;i < n ; i ++ ) {
                  for( int j = 0 ; j < n ; j ++ ) sc(iAr[i][j]);
            }
            
            printf("%d\n" , calc(n, n));
      }
      return 0;
}