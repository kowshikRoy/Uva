//
//  main.cpp
//  106 - Fermat vs. Pythagoras
//
//  Created by Repon Macbook on 6/22/14.
//  Copyright (c) 2014 Repon Macbook. All rights reserved.
//

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<map>
#include<queue>
#include<stack>
#include<cstdlib>
#include<algorithm>
#include<set>
#include<iterator>
#include<cassert>
#include <sstream>
using namespace std;

/*------- Constants---- */
#define MX 1030
#define ll long long
#define ull unsigned long long
#define mod 1000000007

/* -------Global Variables ------ */
/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))


/*------ template functions ------ */
template < class T > inline T gcd(T a , T b ) { if(b==0) return a; else return gcd(b, a%b);}

int ara[1000001];
int main()
{
    
    int n,cnt,z,a,x,y,b,counter;
    while (scanf("%d",&n)==1) {
        cnt=0;
        for (x=2; x*x<n; x++) {
            for (y=1; y<x && x*x+y*y<=n; y++) {
                if((x&1)^(y&1)){
                    z = x*x + y*y;
                    a = x*x - y*y;
                    b = 2*x*y;
                    if(gcd(z,a)==1){
                        cnt++;
                    }
                    ara[z]=1;
                    ara[a]=1;
                    ara[b]=1;
                    for (int i=2; z*i<=n; i++) {
                        ara[z*i]=1;
                        ara[a*i]=1;
                        ara[b*i]=1;
                    }
                }
            }
        }
        counter=0;
        for (int i=1; i<=n; i++) {
            if(ara[i]){
                counter++;
                ara[i]=0;
            }
        }
        cout<<cnt<<" "<<n-counter<<endl;
        //ms(ara, 0);
    }
    return 0;
}
