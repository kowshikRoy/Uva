//
//  main.c
//  102 - Ecological Bin Packing
//
//  Created by MAC on 12/2/13.
//  Copyright (c) 2013 MAC. All rights reserved.
//

#include <stdio.h>
int array[3][3];
char str[6][3]={"BCG","BGC","CBG","CGB","GBC","GCB"};
char str2[6][3]={"021","012","201","210","102","120"};
int main(int argc, const char * argv[])
{

    int i,max,sum,index,kml;
    while (scanf("%d %d %d %d %d %d %d %d %d",&array[0][0],&array[0][1],&array[0][2],&array[1][0],&array[1][1],&array[1][2],&array[2][0],&array[2][1],&array[2][2])==9) {
        sum = array[0][0]+array[0][1]+array[0][2]+array[1][0] + array[1][1] + array[1][2] + array[2][0]+ array[2][1]+ array[2][2];
        max =0;
        index =0;
        for (i=0; i<6; i++) {
            kml = array[0][str2[i][0]-'0'] +  array[1][str2[i][1]-'0'] +  array[2][str2[i][2]-'0'];
            if (kml>max) {
                max = kml;
                index = i;
            }
        }
        sum = sum-max;
        for (i=0; i<3; i++) {
            printf("%c",str[index][i]);
        }
        printf(" %d\n",sum);
    }
    return 0;
}

