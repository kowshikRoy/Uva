//
//  main.cpp
//  107 - The Cat in the Hat
//
//  Created by MAC on 4/9/14.
//  Copyright (c) 2014 MAC. All rights reserved.
//

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<map>
#include<queue>
#include<stack>
#include<cstdlib>
#include<algorithm>
#include<set>
#include<iterator>
#include<cassert>

using namespace std;

/*------- Constants---- */
#define MX 1030
#define ll long long
#define ull unsigned long long
#define mod 1000000007

/* -------Global Variables ------ */
ll x,y;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))


/*------ template functions ------ */
template < class T > inline T gcd(T a , T b ) { if(b==0) return a; else return gcd(b, a%b);}
template < class T > inline T lcm(T a , T b ) { return  a*b / gcd(a, b);}
template < class T > inline T extended_euclid_returning_gcd(T a,T b){ T t,d; if(b==0){d = a;x=1;y=0;} else {extended_euclid_returning_gcd(b, a%b); t=x; x=y;y = t-(a*y/b);}}
template < class T > inline T absolute(T a ) { if(a>0) return a; else return -a;}
template < class T > inline T reverse_num( T a ){ T x=0,tmp = a; while(tmp) { x=10*x+ tmp % 10; tmp/=10;} return x;}
template <class T > inline T big_mod(T base, T power){ T res=1; while (power) { if(power&1){ res*=base; power--;} base =base *base; power/=2;} return res;}

ll kut(ll b,ll p)
{
    ll k=1;
    while (p--) {
        k*=b;
    }
    return k;
}
int main()
{
    ll a,b,i,ln,sum,totalHeight,piku;
    while (scanf("%lld %lld",&a,&b)==2) {
        if(a+b==0) return 0;
        sum=0;
        totalHeight=0;
        piku=1;
        if(b == 1){
            while (a>1) {
                sum++;
                totalHeight+= a;
                a/=2;
                
            }
            printf("%lld %lld\n",sum,totalHeight+1);
            
        }
        else {
            for (ln=2; ; ln++) {
                i= log(b)/log(ln)+.01;
                if(kut(ln, i)==b){
                    if(kut(ln+1, i)==a) break;
                }
            }
            
            for(int j=1;j<=i;j++){
                sum+= piku;
                totalHeight+= piku*a;
                piku*=ln;
                a/=(ln+1);
            }
            printf("%lld %lld\n",sum,totalHeight+b);
        }
    }
    return 0;
}