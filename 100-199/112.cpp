#include <iostream>
#include <sstream>
#include <cstdio>
#include <climits>
#include <ctime>
#include <cstring>
#include <cstdlib>
#include <string>
#include <stack>
#include <map>
#include <cmath>
#include <vector>
#include <queue>
#include <algorithm>
#define  esp 1e-6
#define  pi acos(-1.0)
#define  inf LLONG_MAX
#define  pb push_back
#define  lson l, m, rt<<1
#define  rson m+1, r, rt<<1|1
#define  mp(a, b) make_pair((a), (b))
#define  in  freopen("solve_in.txt", "r", stdin);
#define  out freopen("solve_out.txt", "w", stdout);
#define  bug puts("********))))))");
#define  inout in out
#define  stop  system("pause");
#define  PRD(a) printf("%d\n",(a))
#define  PRU(a) printf("%u\n", (a))
#define  PRID(a) printf("%I64d\n", (a))
#define  PRIU(a) printf("%I64u\n", (a))

#define  PRLU(a) printf("%llu\n", (a))
#define  PRLD(a) printf("%lld\n", (a))

#define  SET(a, v) memset(a, (v), sizeof(a))
#define  READ(a, n) {REP(i, n) cin>>(a)[i];}
#define  REP(i, n) for(int i = 0; i < (n); i++)
#define  Rep(i, base, n) for(int i = base; i < n; i++)
#define  REPS(s, i) for(int i = 0; s[i]; i++)
#define  pf(x) ((x)*(x))
#define  Log(a, b) (log((double)b)/log((double)a))
#define Srand() srand((int)time(0))
#define random(number) (rand()%number)
#define random_range(a, b) (int)(((double)rand()/RAND_MAX)*(b-a) + a)

using namespace std;
typedef long long  LL;
typedef unsigned long long ULL;
typedef vector<int> VI;
typedef pair<int,int> PII;
typedef vector<PII> VII;
typedef vector<PII, int> VIII;
typedef VI:: iterator IT;
typedef map<string, int> Mps;
typedef map<int, int> Mpi;
typedef map<int, PII> Mpii;
typedef map<PII, int> Mpiii;

const int maxn = 5000;

struct NODE {
      LL v;
      NODE *left, *right;
} Node[maxn*maxn];
int ct, flag;
LL tag;
char ch;

NODE *newnode(LL v)
{
      NODE *u = &Node[ct++];
      u->v = v;
      u->right = u->left = NULL;
      return u;
}

void read(LL &v)
{
      int k = 1;
      while(1) {
            ch = getchar();
            if(ch == '-') {
                  k = -1;
                  continue;
            }
            if(isdigit(ch)) {
                  if(v == inf)
                        v = 0;
                  v = v*10 + ch - '0';
            } else if(ch == '(' || ch == ')')
                  break;
      }
      v *= k;
}

NODE *dfs(LL sum)
{
      LL v = inf;
      read(v);
      if(v == inf)
            return NULL;
      NODE *u = newnode(sum + v);
      u->left = dfs(sum+v);
      while(getchar() != '(') ;
      u->right = dfs(sum+v);
      while(getchar() != ')') ;
      if(u->left == NULL && u->right == NULL && sum+v == tag)
            flag = 1;
      return u;
}

void work()
{
      while(1) {
            ct = flag = 0;
            tag = inf;
            read(tag);
            NODE *root = dfs(0);
            if(flag && root) cout<<"yes"<<endl;
            else             cout<<"no"<<endl;
            while((ch = getchar()) != EOF) if(isdigit(ch) || ch == '-') break;
            if(ch == EOF) break;
            else ungetc(ch, stdin);
      }
}

int main()
{
      
      work();
      return 0;
}