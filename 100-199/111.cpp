//
//  main.cpp
//  111 - History Grading
//
//  Created by MAC on 3/11/14.
//  Copyright (c) 2014 MAC. All rights reserved.
//

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<map>
#include<queue>
#include<stack>
#include<cstdlib>
#include<algorithm>
#include<set>
#include<iterator>
#include<cassert>

using namespace std;

/*------- Constants---- */
#define MX 40
#define ll long long
#define ull unsigned long long
#define mod 1000000007

/* -------Global Variables ------ */
ll x,y;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))


/*------ template functions ------ */
template < class T > inline T gcd(T a , T b ) { if(b==0) return a; else return gcd(b, a%b);}
template < class T > inline T lcm(T a , T b ) { return  a*b / gcd(a, b);}
template < class T > inline T extended_euclid_returning_gcd(T a,T b){ T t,d; if(b==0){d = a;x=1;y=0;} else {extended_euclid_returning_gcd(b, a%b); t=x; x=y;y = t-(a*y/b);}}
template < class T > inline T absolute(T a ) { if(a>0) return a; else return -a;}
template < class T > inline T reverse_num( T a ){ T x=0,tmp = a; while(tmp) { x=10*x+ tmp % 10; tmp/=10;} return x;}
template <class T > inline T big_mod(T base, T power){ T res=1; while (power) { if(power&1){ res*=base; power--;} base =base *base; power/=2;} return res;}

int ara[MX],ans[MX],dp[MX];

int func(int n)
{
    int an=1,tmp,pos=ara[ans[n]];
    for (int i=1; i<n; i++) {
        if(ara[ans[i]]<pos){
            tmp=1+dp[i];
            if(tmp>an) an=tmp;
        }
    }
    return an;
}

int main()
{
    int n,i,j,maxima,tmp;
    scanf("%d",&n);
    for (i=1; i<=n; i++) {
        scanf("%d",&ara[i]);
    }
    while (scanf("%d",&i)==1) {
        j=1;
        ms(dp, 0);
        ans[i]=j;
        j++;
        for (; j<=n; j++) {
            scanf("%d",&tmp);
            ans[tmp]=j;
        }
        dp[1]=1;
        maxima=1;
        for(i=1;i<=n;i++){
            dp[i]=func(i);
            if(dp[i]>maxima)maxima=dp[i];
        }
        printf("%d\n",maxima);
    }
    return 0;
}