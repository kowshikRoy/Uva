#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

const double EPS = 1e-10;

typedef double T;
typedef vector<T> VT;
typedef vector<VT> VVT;


double Gauss(VVT &a)
{
    // AX = B;
    // Here n -> # of equations, m = number of column.The last column holds B.
    // The function returns the rank of the matrix
    
    
    int n = a.size(), m= a[0].size();
    int r = 0;
    double det = 1;
    for(int c = 0; c < m  && r < n ; c ++ ) {
        int j = r;
        for(int i = r+1; i < n; i ++ ) {
            if(fabs(a[i][c]) > fabs(a[j][c])) j= i;
        }
        if(fabs(a[j][c]) < EPS) {
            return 0;
        }
        if(r!=j) det *=-1;
        swap(a[j],a[r]);
        T s = 1./a[r][c];
        det *= a[r][c];
        for(int i = 0; i < m ; i ++ ) a[r][i] *= s;
        for(int i=0;i < n ;i ++) if(i!=r) {
            T t = a[i][c];
            for(int j = 0; j < m ; j ++ ) a[i][j] -= t * a[r][j];
        }
        r++;
    }
    
    // Now det = Determinant of the matrix
    // r = Rank of the matrix
    return det;
    
    
}

int n;
int main() {

    while(scanf("%d",&n)==1 && n) {
        VVT a(n,VT(n,0));
        for(int i =0; i< n;i++)
            for(int j = 0; j < n ; j ++ )
                scanf("%lf",&a[i][j]);
        VVT b;
        double det = Gauss(a);
        
        if(det>=0)
            det=floor(det+0.5);
        else
            det=-floor(-det+0.5);
        printf("%.0lf\n",det);
        
    }
    printf("*\n");
    

    
}