#include<bits/stdc++.h>
using namespace std;
const int N = 55;
int n,T;
int p[N];

pair<int,int> Merge(pair<int,int> a, pair<int,int>b)
{
    if(a.first == b.first) {
        if(a.second > b.second) return a;
        else return b;
    }
    return (a.first > b.first ? a : b);
}

int main()
{
    int t,cs=0;
    cin>>t;
    while(t--){
        cin >> n >> T;
        int sum = 0;
        for(int i = 0;i < n ;i ++) {
            cin >> p[i];
            sum += p[i];
        }
        vector<pair<int,int> > dp(sum+1,make_pair(0,0));
        for(int i = 0; i < n;i ++ ) {
            for(int j= sum - p[i];j >= 0 ;j -- ){
                if(j + p[i] <= sum ) {
                    pair<int,int> o= make_pair(dp[j].first + 1, dp[j].second + p[i]);
                    dp[j + p[i]] = Merge(dp[j+p[i]],o);
                }
            }
        }
        pair<int,int> ans = make_pair(0,0);
        for(int i = 0; i <= min(sum, T-1); i ++) {
            ans = Merge(ans,dp[i]);
        }
        printf("Case %d: %d %d\n",++cs, ans.first + 1, ans.second + 678);
    }
}