import sys
def power(b,e):
    if(e==0):
        return 1
    if (e%2):
        return b * power(b,e-1)
    a = power(b,e/2);
    return a*a;


for line in sys.stdin:
    n,k=map(int,line.split())
    dp = [0 for i in range(n+1)]
    dp[k] = 1
    for i in range(k+1,n+1) :
        dp[i] = 2*dp[i-1] + power(2,i-k-1) - dp[i-k-1]
    print(dp[n])