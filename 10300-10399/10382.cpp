#include <bits/stdc++.h>
using namespace std;
const int N = 1e4+4;
int n,l,w;
struct Seg{
    double l,r;
    bool operator<(const Seg &s) const {
        return r < s.r;
    }
};
vector<Seg>S;
bool cmp(Seg a,Seg b)
{
    return a.r < b.r;
}
int dp[N];
int T[4*N];
void build(int n,int b,int e)
{
    if(b==e) {
        T[n] = 1e6;
        return;
    }
    int mid = (b+e)/2;
    build(2*n,b,mid);
    build(2*n+1,mid+1,e);
    T[n] = min(T[2*n],T[2*n+1]);
}

void update(int n,int b,int e,int pos,int v)
{
    if(b==e && b== pos){
        T[n] = v;
        return;
    }
    int mid = (b+e)/2;
    if(pos<=mid) update(2*n,b,mid,pos,v);
    else update(2*n+1,mid+1,e,pos,v);
    T[n] = min(T[2*n],T[2*n+1]);
}

int query(int n,int b,int e,int i,int j)
{
    if(b>j||e<i) return 1e8;
    if(b>=i&&e<=j) return T[n];
    int mid = (b+e)/2;
    return min(query(2*n,b,mid,i,j), query(2*n+1, mid+1,e,i,j));
}
int main()
{
    while(scanf("%d %d %d",&n,&l,&w)==3) {
        for(int i = 0; i < n; i ++ ) {
            double x,r;
            scanf("%lf %lf",&x,&r);
            if(r*r-w*w/4. < 0) continue;
            Seg p;
            p.l = x - sqrt(r*r-w*w/4.);
            p.r = x + sqrt(r*r-w*w/4.);
            S.push_back(p);
        }
        sort(S.begin(), S.end(),cmp);
        bool flag = 0;
        int id = -1;
        
        double prv = -1;
        for(int i = 0; i < S.size() ;i ++) {
            if(S[i].l <= 0 ) {
                if(S[i].r > 0 && S[i].r > prv) {
                    prv = S[i].r;
                    flag = 1;
                    id = i;
                }
            }
        }
        if(flag== 0){
            printf("-1\n");
            
        }
        else {
            for(int i = 0;i <id; i++ ) {
                dp[i] = 1e8;
                update(1,0,n-1,i,dp[i]);
            }
            dp[id] = 1;
            update(1,0,n-1,id,dp[id]);
            for(int i = id + 1; i < S.size(); i ++ ) {
                Seg p = {-1,S[i].l};
                int k = lower_bound(S.begin(), S.end(),p) - S.begin();
                if(k>=i) {
                    dp[i] = 1e8;
                    update(1,0,n-1,i,dp[i]);
                    continue;
                }
                int o = query(1,0,n-1,k,i-1);
                dp[i] = o+1;
                update(1,0,n-1,i,dp[i]);
            }
            int ans = 1e8;
            for(int i = 0; i < S.size(); i ++) {
                if(S[i].l <= l && S[i].r >= l){
                    ans = min(ans, dp[i]);
                }
            }
            if(ans > n) {
                printf("-1\n");
            }
            else printf("%d\n",ans );
        }
        S.clear();
    }
}
/*
 8 20 2
 5 3
 4 1
 1 2
 7 2
 10 2
 13 3
 16 2
 19 4
 3 10 1
 3 5
 9 3
 6 1
 3 10 1
 5 3
 1 1
 9 1
 */