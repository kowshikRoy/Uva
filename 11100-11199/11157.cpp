/*
Category: Maxflow
Solution: Binary Search over the answer and check we maxflow >=2, Create graph
Maintaining Properties
*/

/*
 
 Maxflow Dinic Implementation. Work for both directed and Undirected Graph
 Complexity : V^2E
 
 * init(number_of_node)
 * addEdge(from, to, cap)
 * dinitz(source,dest)
 
 $ MAXNODE : 3000
 $ For Undirected Graph : edge and reverse edge capacity = cap
 
 */
#include<bits/stdc++.h>
using namespace std;

const int N = 505;
typedef long long T;
struct Edge
{
    int u, v;
    T cap, flow;
    Edge(int u, int v, T c, T f):u(u), v(v), cap(c), flow(f) {}
};

struct Dinic
{
    int n, m, s, t;
    const T oo = 1e9;
    vector<Edge> edge;
    vector<int> G[N];
    bool vis[N];
    int d[N];
    int cur[N];
    
    void init(int n)
    {
        this->n=n;
        for(int i=0; i<=n; i++)
            G[i].clear();
        edge.clear();
    }
    
    void addEdge(int u, int v, T cap)
    {
        edge.push_back(Edge(u, v, cap, 0));
        edge.push_back(Edge(v, u, 0, 0));
        m=edge.size();
        G[u].push_back(m-2);
        G[v].push_back(m-1);
    }
    
    bool bfs()
    {
        memset(vis, 0, sizeof vis);
        queue<int> q;
        q.push(s);
        d[s]=0;
        vis[s]=1;
        while(!q.empty())
        {
            int x=q.front();
            q.pop();
            for(int i=0; i<G[x].size(); i++)
            {
                Edge& e=edge[G[x][i]];
                if(!vis[e.v] && e.cap>e.flow)
                {
                    vis[e.v]=true;
                    d[e.v]=d[x]+1;
                    q.push(e.v);
                }
            }
        }
        return vis[t];
    }
    
    T dfs(int x, T a)
    {
        if(x==t || a==0)return a;
        T flow=0, f;
        for(int& i=cur[x]; i<G[x].size(); i++)
        {
            Edge& e=edge[G[x][i]];
            if(d[x]+1==d[e.v] && (f=dfs(e.v, min(a, e.cap-e.flow)))>0)
            {
                e.flow+=f;
                edge[G[x][i]^1].flow-=f;
                flow+=f;
                a-=f;
                if(a==0)break;
            }
        }
        return flow;
    }
    
    T dinitz(int s, int t)
    {
        this->s=s;
        this->t=t;
        T flow=0;
        while(bfs())
        {
            memset(cur, 0, sizeof cur);
            flow+=dfs(s, oo);
        }
        return flow;
    }
} maxf;

pair<bool,long long> v[N];
long long D,n;

bool can(long long M)
{
    maxf.init(2*n+2);
    for(int i = 1; i <= n; i ++ ) {
        for(int j = i+1; j <= n; j ++ ) {
            if(v[j].second - v[i].second <= M) {
                maxf.addEdge(n+i,j,100);
            }
        }
        if(v[i].first) maxf.addEdge(i,n+i,1);
        else maxf.addEdge(i,n+i,2);
    }
    for(int i = 1; i <= n; i ++ ) {
        if(v[i].second <= M ) maxf.addEdge(0,i,100);
        if(D - v[i].second <= M) maxf.addEdge(n+i, 2*n+1, 100);
    }
    if(D <= M) maxf.addEdge(0,2*n+1,100);
    int k = maxf.dinitz(0,2*n+1);
    return k>=2;
}
int main()
{
    int t,cs=0;
    long long  d ;
    char ch;
    scanf("%d",&t);
    while(t--) {
        scanf("%lld %lld",&n,&D);
        for(int i = 1; i <= n; i ++ ) {
            scanf(" %c-%lld",&ch,&d);
            v[i] = make_pair(ch=='S',d);
        }
        
        long long low = 0, high = D, mid , ans = -1;
        while(low <= high) {
            mid = (low + high) / 2;
            if(can(mid)) {
                ans = mid;
                high = mid-1;
            }
            else low = mid+1;
        }
        printf("Case %d: %lld\n",++cs, ans);
    }
}

