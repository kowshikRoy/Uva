#include<bits/stdc++.h>
using namespace std;
const int N = 2005;
struct PT{
    int x,y;
}p[N*N], q[N*N];
int n,m;
int vis[N*N];
int dest[N*N];
int d[N*N];
int cs = 1;
int dir[][2] = {{0,1},{0,-1},{1,0},{-1,0}};

bool check(int x,int y)
{
    if(x>=0&&x<=2000 && y>=0 && y <= 2000 )return 1;
    return 0;
}
int bfs()
{
    for(int i = 0; i < n; i ++ ) dest[p[i].x * N + p[i].y] = cs;
    queue<int>qu;
    for(int i= 0;i < m ;i++) {
        qu.push(q[i].x * N + q[i].y);
        d[q[i].x * N + q[i].y] =  0;
        vis[q[i].x * N + q[i].y] = cs;
    }
    int ans = -1;
    while(!qu.empty()){
        int u = qu.front();
        qu.pop();
        if(dest[u] == cs){
            ans = d[u];
            break;
        }
        int x = u/N, y = u% N;
        for(int i = 0; i < 4;  i ++ ) {
            int xp = x + dir[i][0];
            int yp = y + dir[i][1];
            int id = xp * N + yp;
            if(check(xp,yp) && vis[id] != cs ) {
                d[id] = 1 + d[u];
                vis[id] = cs;
                qu.push(id);
            }
        }
    }
    return ans;
    
}
int main()
{
    while(scanf("%d",&n)==1 && n ){
        for(int i = 0; i < n; i ++) scanf("%d %d",&p[i].x, &p[i].y);
        scanf("%d",&m);
        for(int i = 0; i < m; i ++) scanf("%d %d",&q[i].x, &q[i].y);
        printf("%d\n",bfs());
        cs ++;
        
    }
}