#include <bits/stdc++.h>
using namespace std;
const int N = 700;
int n,a[35],p[35];
int vis[33][N][N];
int cs = 1;
int dp[33][N][N];
int solve(int ind,int p1,int p2)
{
    int p3 = (ind > 0 ? p[ind-1] : 0) - p1 - p2;
    int ar[3];
    ar[0]=p1,ar[1]=p2,ar[2]=p3;
    sort(ar,ar+3);
    p1 = ar[0];
    p2 = ar[1];
    p3 = ar[2];
    //printf("%d %d %d\n",p1,p2,p3);
    if(ind == n) {
        return p3-p1;
    }
    
    if (vis[ind][p1][p2] == cs ) return dp[ind][p1][p2];
    int res = 1e8;
    res = solve(ind + 1, p1 + a[ind] , p2);
    res = min(res, solve(ind + 1, p1, p2 + a[ind]));
    res = min(res, solve(ind + 1, p1, p2));
    vis[ind][p1][p2] = cs;
    return dp[ind][p1][p2] = res;
    
    
}
int main()
{
    
    int t;
    scanf("%d",&t);
    while(t--) {
        scanf("%d",&n);
        for(int i = 0; i < n; i ++) {
            scanf("%d",&a[i]);
            p[i] = a[i] + (i>0?p[i-1] : 0);
        }
        printf("Case %d: %d\n",cs++, solve(0,0,0));
    }
}