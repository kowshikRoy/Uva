#include <bits/stdc++.h>
using namespace std;
int a[100005];
int main()
{
    int t,n,l,cs=0;
    scanf("%d",&t);
    while(t--){
        scanf("%d %d",&n,&l);
        for(int i=0 ;i <n ; i ++) {
            scanf("%d",&a[i]);
        }
        sort(a,a+n);
        deque<int>dq;
        for(int i = 0; i < n; i ++ ) dq.push_back(a[i]);
        int cnt= 0;
        while(dq.size()) {
            int p = dq.back();dq.pop_back();
            if(dq.size() && dq.front() + p <= l) {
                dq.pop_front();
            }
            cnt ++;
        }
        if(cs)printf("\n");
        cs =1;
        printf("%d\n",cnt);
        
    }
}