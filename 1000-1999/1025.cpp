#include<bits/stdc++.h>
using namespace std;

const int N = 55;
int n,T,t[N],d,m1,m2,cs=0;
bool flag = 0;
bool hasTrain[205][55][2];
int dp[55][205];
int calc(int ind,int Time)
{
    if(Time > T) return 1e8;
    if(ind == n) {
        flag = 1;
    }
    if(dp[ind][Time] !=-1) return dp[ind][Time];
    int res = 1e8;
    if(ind == n) res = T- Time;
    res= min(res, 1 + calc(ind,Time+1));

    if(ind < n && hasTrain[Time][ind][0] ) res = min(res,  calc(ind+1, Time + t[ind]));
    if(ind >= 2 && hasTrain[Time][ind][1] ) res = min(res , calc(ind-1, Time + t[ind-1]));
    return dp[ind][Time] = res;
}
int main()
{
    while(scanf("%d",&n)==1 &&n ) {
        scanf("%d",&T);
        for(int i = 1; i <n ;i ++) scanf("%d",&t[i]);
        scanf("%d",&m1);
        for(int i=0;i<m1;i++){
            scanf("%d",&d);
            for(int i = 1; i < n; i ++) {
                if(d <= T)hasTrain[d][i][0] = 1;
                    d += t[i];
            }
        }
        scanf("%d",&m2);
        for(int i = 0;i<m2; i ++) {
            scanf("%d",&d);
            for(int i = n; i > 1; i -- ) {
                if(d<=T)hasTrain[d][i][1] = 1;
                d+=t[i-1];
                
            }
        }
        printf("Case Number %d: ",++cs);
        memset(dp,-1,sizeof dp);
        flag = 0;
        int p = calc(1,0);
        if(flag) printf("%d\n",p);
        else printf("impossible\n");
        
        memset(hasTrain, 0, sizeof hasTrain);
    }
}