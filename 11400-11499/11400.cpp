/*
    Sort the categories by voltage. 
    Run through this list. 
    At each category you can either buy all the lamps you need now 
    (hence you also buy the source for this category), 
    or you can choose not to buy anything now and hence pass on all the lamps from this category 
    and whatever you did not buy before to the next. If you think about it, this
    defines a 2D state space (the current category and when you bought lamps last time). 
*/

#include<bits/stdc++.h>
using namespace std;

const int N = 1005;
struct cat{
    int volt,cost,lcost,amo;
    bool operator<(const cat &p) const {
        return volt < p.volt;
    }
}p[N];
int n;
int cs = 1;
long long dp[N][N];
int VIS[N][N];
int csum[N];

long long F(int ind,int prv)
{
    if(ind == n) return 0;
    if(prv >= 0 && VIS[ind][prv] == cs) return dp[ind][prv];
    long long res = (ind== n-1 ? 1e9 : F(ind+1,prv));
    long long res2 = p[ind].cost + p[ind].lcost * (csum[ind] - (prv>=0 ? csum[prv]: 0)) + F(ind+1,ind);
    VIS[ind][prv] = cs;
    return dp[ind][prv] = min(res,res2);
    
}
int main()
{
    
    while(scanf("%d",&n)==1 && n ) {
        for(int i = 0; i < n; i ++ ) {
            scanf("%d %d %d %d",&p[i].volt, &p[i].cost ,& p[i].lcost, &p[i].amo);
        }
        sort(p,p+n);
        csum[0] = p[0].amo;
        for(int i = 1;i <n;i ++) csum[i] = csum[i-1] + p[i].amo;
        
        printf("%lld\n",F(0,-1));
        cs++;
    }
}