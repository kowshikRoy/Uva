#include <bits/stdc++.h>
using namespace std;

const int N = 1e6+6;
const int SIGMA = 52;
int Map[1005];
int idx[150];
namespace Aho{
    int to[N][SIGMA], link[N] , go;
    int nc[N];
    vector<int>VQ;
    void init()
    {
        memset(link,0,sizeof link);
        memset(to,0,sizeof to);
        memset(nc,0,sizeof nc);
        VQ.clear();
        go = 1;
    }
    void insert(char s[],int k)
    {
        int cur = 0;
        for(int i= 0; s[i];i ++ ) {
            int c = idx[s[i]];
            if(!to[cur][c]){
                to[cur][c] = go ++;
            }
            cur = to[cur][c];
        }
        Map[k] = cur;
    }
    void fail()
    {
        queue<int>q;
        for(int i = 0 ; i< 52; i ++) if(to[0][i]) q.push(to[0][i]);
        while(!q.empty()){
            int u = q.front(); q.pop();
            for(int i = 0; i < 52; i ++ ) {
                if(!to[u][i]) continue;
                int v = to[u][i] , j = link[u];
                while(j > 0 && ! to[j][i] ) j = link[j];
                if(to[j][i]) j = to[j][i];
                link[v] = j;
                q.push(v);
                VQ.push_back(v);
            }
        }
    }
    void find(char s[])
    {
        for(int i = 0, j = 0; s[i] ;  i ++ ) {
            int c = idx[s[i]];
            while(j>0&&!to[j][c]) j = link[j];
            if(to[j][c]) j = to[j][c];
            nc[j] ++;
        }
    }
    void calc()
    {
        reverse(VQ.begin(), VQ.end());
        for(int i = 0; i < VQ.size(); i ++ ) {
            nc[link[VQ[i]]] += nc[VQ[i]];
        }
    }
    
};
char T[100006];
char s[1005];

int main()
{
    for(int i = 'a'; i <='z'; i ++) idx[i] = i-'a';
    for(int i = 'A'; i <='Z'; i ++) idx[i] = i-'A' + 26;
    int t,cs=0,n;
    scanf("%d",&t);
    while(t--){
        
        scanf("%s",T);
        scanf("%d",&n);
        Aho::init();
        for(int i = 0; i < n; i ++ ){
            scanf("%s",s);
            Aho::insert(s,i);
        }
        Aho::fail();
        Aho::find(T);
        Aho::calc();
        //printf("Case %d:\n",++cs);
        for(int i = 0; i < n ; i ++ ) printf("%c\n", Aho::nc[Map[i]] ? 'y' : 'n');
    }
    
}