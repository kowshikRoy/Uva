#include<bits/stdc++.h>
using namespace std;

const int N = 1001;
int n,n1,n5,n10,cs=1;
int dp[155][200][55];
int org;
int solve(int ind,int five,int ten )
{
    if(ind==n) {
        return 0;
    }
    int one = org - five * 5 - ten * 10 - ind * 8 ;
    if(dp[ind][five][ten] !=-1) return dp[ind][five][ten];
    int res = 1e8;
    if(one >= 8) res  = 8 + solve(ind+1, five,ten);
    if(ten >= 1) res  = min(res, 1 + solve(ind+1,five, ten-1));
    if(five >= 2) res = min(res, 2 + solve(ind + 1, five - 2, ten ));
    if(five >= 1 && one >= 3) res = min(res, 4 + solve(ind+1,five-1, ten) );
    if(ten>=1 && one>=3) res = min(res, 4 + solve(ind + 1, five + 1 , ten - 1 ));
    return dp[ind][five][ten]  = res;
}
int main()
{
    //freopen("/Users/MyMac/Desktop/in.txt","r",stdin);
    int t;
    scanf("%d",&t);
    while(t--){
        scanf("%d %d %d %d",&n,&n1,&n5,&n10);
        org = n1 + n5*5 + n*10 ;
        memset(dp,-1,sizeof dp);
        printf("%d\n",solve(0,n5,n10));
    }
    return 0;
}