n=150
b=150
dp = [[-1 for i in range (b) ]
       for j in range (b)]

def solve(ind,prv):
    if ind == n:
        return 1
    if(dp[ind][prv] !=-1):
        return dp[ind][prv]
    res = 0
    for i in range(0, b):
        if prv == 1 and i == 3:
            continue
        if ind == 0 and i == 0:
            continue
        res += solve(ind + 1, i)
    dp[ind][prv] = res
    return res

while(1):
    b,n=map(int,input().split())
    if(b==0and n==0):
        break
    for i in range (150):
        for j in range (150):
            dp[i][j]=-1
    print(solve(0,b))


