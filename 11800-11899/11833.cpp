#include<bits/stdc++.h>
using namespace std;
/*------- Constants---- */

#define Long                    long long
#define Ulong                   unsigned long long
#define forn(i,n)               for( int i=0 ; i < n ; i++ )
#define mp(i,j)                 make_pair(i,j)
#define pb(a)                   push_back((a))
#define SZ(a)                   (int) a.size()
#define all(x)                  (x).begin(),(x).end()
#define PI                      acos(-1.0)
#define EPS                     1e-9
#define xx                      first
#define yy                      second
#define lc                      ((n)<<1)
#define rc                      ((n)<<1|1)
#define db(x)                   cout << #x << " -> " << x << endl;
#define Di(x)                   int x;scanf("%d",&x)
#define min(a,b)                ((a)>(b) ? (b) : (a) )
#define max(a,b)                ((a)>(b) ? (a):(b))
#define ms(ara_name,value)      memset(ara_name,value,sizeof(ara_name))

/*************************** END OF TEMPLATE ****************************/

const int N = 255;
int d[N][N];
int dist[N];
int n, m, c, k;
int dk[N];
vector<pair<int,int> >G[N];
struct Node
{
    int u, w;
    bool operator<(const Node &p) const {
        return w > p.w;
    }
};
void dij(int s)
{
    memset(dk, 63, sizeof dk);
    int dst = c - 1;
    priority_queue<Node>pq;
    dk[s] = 0;
    pq.push({s, dk[s]});
    
    while (!pq.empty()) {
        Node u = pq.top(); pq.pop();
        if(u.u==dst) {
            printf("%d\n",u.w);
            return;
        }
        for (int i = 0; i < G[u.u].size(); i ++) {
            int v = G[u.u][i].first, w = G[u.u][i].second;
            if(dk[v] >  dk[u.u] + w ) {
                dk[v] = dk[u.u] + w;
                if(v < c ) {
                   
                    int o = dk[v] + dist[v];
                    pq.push({c-1,o});
                }
                else {
                    
                    pq.push({v,dk[v]});
                }
            }
        }
        
    }
}
int main()
{
 
    
    while (scanf("%d %d %d %d", &n, &m, &c, &k) == 4) {
        if(n==0&&m==0&&c==0&&k==0) break;
        memset(d, 63, sizeof d);
        for(int i = 0; i < n; i ++) G[i].clear();
        for (int i = 0; i < m ; i ++) {
            int a, b, c;
            scanf("%d %d %d",&a,&b,&c);
            d[a][b] = min(d[a][b], c);
            d[b][a] = d[a][b];
            G[a].push_back(make_pair(b, c));
            G[b].push_back(make_pair(a, c));
        }
        memset(dist,63,sizeof dist);
        dist[c - 1] = 0;
        for (int i = c - 2; i >= 0; i -- ) {
            dist[i] = dist[i + 1] + d[i][i + 1];
        }
        
        dij(k);
    }
    return 0;
}