#include <bits/stdc++.h>
using namespace std;
const int N = 25e4+4;
char word[N][17];
map<string,int>Map;
vector<int>G[N];
int dp[N];
int dfs(int u)
{
    
    if(dp[u]!=-1) return dp[u];
    int res = 1;
    for(auto a: G[u]) {
        res = max(res, 1 + dfs(a));
    }
    return dp[u] = res;
}
int main()
{
    //freopen("/Users/MyMac/Desktop/in.txt","r",stdin);
    int n=0;
    while(scanf("%s",word[n])==1){
        Map[word[n]] = n;
        n++;
    };
    for(int k =0;k <n ;k ++) {
        string p = word[k];
        //Adding
        for(int i = 0; i < 26; i ++ ) {
            for(int j = 0; j < p.length()+1; j ++ ) {
                string o = p;
                o.insert(o.begin()+j,i+'a');
                if(Map.count(o) && Map[o] > k) {
                    G[k].push_back(Map[o]);
                }
            }
        }
        // Deleting
        for(int i = 0; i < p.length(); i ++) {
            string o = p;
            o.erase(o.begin()+i);
            if(Map.count(o) && Map[o] > k) {
                G[k].push_back(Map[o]);
            }
        }
        for(int i = 0; i < p.length();i++){
            for(int j = 0; j < 26;j ++ ) {
                string o = p;
                o[i] = j+'a';
                if(Map.count(o) && Map[o] > k) {
                    G[k].push_back(Map[o]);
                }
            }
        }
    }
    memset(dp,-1,sizeof dp);
    for(int i = 0; i < n;i ++ ) {
        if(dp[i]==-1) dp[i] = dfs(i);
    }
    int ans = -1;
    for(int i = 0; i < n; i ++) {
        ans= max(ans, dp[i]);
    }
    printf("%d\n",ans);
    
}