#include<bits/stdc++.h>
using namespace std;
#define F first
#define S second
typedef pair<int,int> Pair;
const int N = 105;

Pair p[N];
int n,m;
int main()
{
    while(scanf("%d %d",&m,&n)==2) {
        for(int i = 0; i < n ;i ++){
            scanf("%d %d",&p[i].F, &p[i].S);
        }
        vector<bool> vis(m+205, 0);
        vector<int> dp(m+205,0);
        vis[0] = 1;
        for(int i = 0; i < n; i ++ ) {
            for(int j = m+200; j>= 0; j -- ) {
                if(j >= p[i].F && vis[j-p[i].F]) {
                    dp[j] = max(dp[j] , p[i].S + dp[j - p[i].F] );
                    vis[j] = 1;
                }
            }
        }
        int ans = 0;
        for(int i = m + 200; i >= 0;i --  ){
            if(vis[i] ==0 )continue;
            if(i > m && i > 2000) {
                ans = max(ans, dp[i]);
            }
            if(i <= m) ans = max(ans, dp[i]);
        }
        printf("%d\n",ans);
    }
}

