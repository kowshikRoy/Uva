#include <bits/stdc++.h>
using namespace std;
vector<int>fac;

const int N = 1e4+5;
bool vis[N];
vector<int> F[N];
void seive()
{
    for(int i = 2; i < N; i ++ ) {
        F[i].push_back(1);
        if(vis[i] == 0 ) {
            for(int j = i;  j < N; j += i)  F[j].push_back(i), vis[j] = 1;
        }
    }
}

int main()
{
    seive();
    int n;
    while(scanf("%d",&n)==1 && n) {
        if(n%2==0) printf("%d %d %d\n",n,n,n/2);
        else {
            //printf("For = %d\n",n);
            for(int x = n; x >= 1 ; x -- ) {
                int up = 4 * x - n;
                int down = x * n;
                if(up < 0) break;
                int k = __gcd(up,down);
                up = up / k , down /= k;
                set<int>S;
                vector<int>temp;
                fac.clear();
                for(auto a: F[x]) {
                    for(auto b : F[n]) {
                        S.insert(a*b);
                    }
                }
                for(auto a: S) if(down % a) temp.push_back(a);
                for(auto a: temp)S.erase(a);
                for(auto a: S) fac.push_back(a);
                bool f = 1;
                
                for(int i = 0; i < fac.size() &&f; i ++ ) {
                    for(int j = i; j < fac.size() && f; j ++ ) {
                        if((fac[i] + fac[j] ) % up == 0) {
                            long long A = fac[i], B  = fac[j];
                            long long D = (A+B)/up;
                            printf("%d %lld %lld\n", x, D * down / A , D * down / B);
                            f = 0;
                            break;
                        }
                    }
                }
                
                if(f==0) break;
                
            }
            
        }
    }
}