#include <bits/stdc++.h>
using namespace std;
const int N = 1205;
#define PI acos(-1.0)
#define EPS 1e-9
struct PT{
    double x,y,ang;
    PT(double x=0,double y=0):x(x),y(y){
        ang= atan2(y,x);
    }
    bool operator<(const PT & p) const {return ang < p.ang;}
}p[N],q[N*2];
int n;
long long calc(int id)
{
    long long m = 0;
    for(int i= 0; i < n; i ++) {
        if(i==id) continue;
        q[m++] = PT(p[i].x-p[id].x , p[i].y - p[id].y);
    }
    sort(q,q+m);
    for(int i = 0; i < m ;i ++) q[i+m] = q[i], q[i+m].ang += 2*PI;
    long long r = 0;
    for(int i = 0 ,j = 1; i < m ; i ++ ) {
        while(q[j].ang - q[i].ang <= PI + EPS) j ++;
        r += (j-i-1) * (j-i-2) /2;
    }
    return m * (m-1) * (m-2) / 6 - r;
}
int main()
{
    int cs = 0;
    while(scanf("%d",&n)==1 && n) {
        for(int i = 0; i <n ; i ++) {
            double x,y;
            scanf("%lf %lf",&x,&y);
            p[i] = PT(x,y);
        }
        long long sum= 0 , tot = n * (n-1) * (n-2) / 6;
        for(int i = 0; i < n ; i ++ ) {
            sum += calc(i);
        }
        printf("City %d: %.2lf\n", ++cs, sum * 1. / tot ) ;
    }
}

