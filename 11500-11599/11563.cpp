#include <bits/stdc++.h>
using namespace std;
const int N= 1e5+5;
int a[N],c,d,n;
int Nxt[N] , Prv[N];
map<int,int>Map;
struct Node{
    int pos,nxt;
    bool operator<(const Node &p ) const {
        if(nxt == p.nxt) return pos < p.pos;
        return nxt > p.nxt;
    }
};
int main()
{
    while(scanf("%d %d %d",&c,&d,&n)==3) {
        for(int i = 0; i < n; i ++) Nxt[i] = n, Prv[i] = -1;
        for(int i = 0;i < n; i ++) {
            scanf("%d",&a[i]);
            if(Map.count(a[i])){
                Nxt[Map[a[i]]] = i;
                Prv[i] = Map[a[i]];
            }
            Map[a[i]] = i;
        }
        set<int>V;
        set<Node>S;
        int cnt = 0;
        for(int i = 0; i < n ;i ++) {
            int p = a[i];
            if(V.count(p)) {
                int in = Prv[i];
                S.erase({in,i});
                S.insert({i,Nxt[i]});
            }
            else {
                cnt ++;
                if(V.size() == c ) {
                    Node o = *S.begin();
                    S.erase(o);
                    V.erase(a[o.pos]);
                }
                V.insert(a[i]);
                S.insert({i,Nxt[i]});
            }
        }
        printf("%d\n",cnt);
        Map.clear();
        
    }
}