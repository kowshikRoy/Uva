#include <bits/stdc++.h>
#include <climits>
using namespace std;
int n;
int dp[1<<8][1<<8];
int grid[8][8];

int calc(int a,int b)
{
    if(a + 1== 1 << n) return 0;
    if(dp[a][b] != -1 ) return dp[a][b];
    int res = INT_MIN;
    for(int i =0 ; i < n ;i ++) {
        if(a & 1 << i) continue;
        bool flag= 0;
        int in = 1e8;
        for(int j = 0; j < n ; j ++ ) {
            if(b & 1<<j) continue;
            int tmp = grid[i][j] + calc(a | 1<<i, b | 1<< j);
            in = min(in, tmp);
            flag = 1;
        }
        if(flag) res = max(res, in);
    }
    return dp[a][b] =res;
}
int main()
{
    int t;
    scanf("%d",&t);
    while(t--) {
        scanf("%d",&n);
        for(int i=0; i < n;i++) {
            for(int j = 0; j< n; j ++) scanf("%d",&grid[i][j]);
        }
        memset(dp,-1,sizeof dp);
        printf("%d\n", calc(0,0));
    }
}