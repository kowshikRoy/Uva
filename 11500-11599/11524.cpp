#include <bits/stdc++.h>
using namespace std;
#define PI acos(-1.0)
double r,m1,n1,m2,n2,m3,n3;


double Tri(double a,double b,double c)
{
    double s = (a+b+c)/2;
    if(a > s || b > s || c > s) return -1;
    return sqrt(s*(s-a)*(s-b)*(s-c));
}

double area;
double calc(double mid)
{
    double BP = mid;
    double AP = m1 *BP/n1;
    double AR = AP;
    double CR = m3 * AR / n3;
    double CQ = CR;
    double BQ = m2 * CQ / n2;
    area =  Tri(BQ + CQ , AR + CR, BP + AP);
    return 2 * area / (BQ + CQ + AR + CR + BP + AP);
}

int main()
{
    int t;
    cin>>t;
    while(t--) {
        scanf("%lf %lf %lf %lf %lf %lf %lf",&r,&m1,&n1,&m2,&n2,&m3,&n3);
        
        double low = 0, high = 1e9 , mid , ans;
        for(int i = 0; i < 100 ; i ++ ) {
            mid = (low+high) / 2;
            double r2 = calc(mid);
            if(r2 < r) low = mid;
            else high = mid;
        }
        calc(low);
        printf("%.4lf\n",area);
    }
}
