//
//  main.cpp
//  11572 - Unique Snowflakes
//
//  Created by utpal roy on 22/08/2014.
//  Copyright (c) 2014 utpal roy. All rights reserved.
//

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<map>
#include<queue>
#include<stack>
#include<cstdlib>
#include<algorithm>
#include<set>
#include<iterator>
#include<cassert>
#include <list>
#include <sstream>
using namespace std;

/*------- Constants---- */
#define MX 130000
#define ll long long
#define ull unsigned long long
#define mod 1000000007

/* -------Global Variables ------ */
ll x,y,d;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))


/*------ template functions ------ */
template < class T > inline T gcd(T a , T b ) { if(b==0) return a; else return gcd(b, a%b);}
template < class T > inline T lcm(T a , T b ) { return  a*b / gcd(a, b);}
template < class T > inline T extended_euclid_returning_gcd(T a,T b){ T t; if(b==0){d = a;x=1;y=0;} else {extended_euclid_returning_gcd(b, a%b); t=x; x=y;y = t-(a*y/b);}}
template < class T > inline T absolute(T a ) { if(a>0) return a; else return -a;}
template < class T > inline T reverse_num( T a ){ T x=0,tmp = a; while(tmp) { x=10*x+ tmp % 10; tmp/=10;} return x;}
template <class T > inline T big_mod(T base, T power){ T res=1; while (power) { if(power&1){ res*=base; power--;} base =base *base; power/=2;} return res;}
list<ll> sik;
list<ll> ::iterator it;

map<ll, ll> klmap;
//map<ll, ll> ::iterator imap;
int main()
{
    
    ll test,n,a,tmp,maxi,i,cursor;
    scanf("%lld",&test);
    while (test--) {
        scanf("%lld",&n);
        maxi=0;
        cursor=1;
        for (i=1;i<=n;i++){
            scanf("%lld",&a);
            //imap = klmap.find(a);
            if(klmap.find(a)==klmap.end()){
                klmap[a]=i;
            }
            else {
                if(klmap[a]>=cursor){
                    cursor = klmap[a]+1;
                    klmap[a] =i;
                    
                }
                else {
                    klmap[a]=i;
                }
                
            }
            if(i-cursor+1>maxi) maxi = i-cursor+1;
            
        }
        cout<<maxi<<"\n";
        klmap.clear();
    }
    return 0;
}
