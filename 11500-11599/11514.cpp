#include <bits/stdc++.h>
using namespace std;
int p,v,e;
const int N = 1005;
char name[N][105], vl[N][105];
int at[N], dmg[N];
int def[N];
char l[1000005];
map<string,int> Map;
int Mat[N][N];
long long dp[N][N];

long long F(int p1, int p2)
{
    if(p1 == p ) {
        if( p2 == v) return 0;
        else return 1e15;
    }
    if(dp[p1][p2] !=-1 )return dp[p1][p2];
    long long res = 1e15;
    if(Mat[p1][p2] && at[p1] >= def[p2]) res = dmg[p1]  + F(p1+1, p2+1);
    res = min(res, F(p1+1,p2));
    return dp[p1][p2] = res;
}

int main()
{
    while(scanf("%d %d %d",&p,&v,&e)==3) {
        if(p==0 &&v == 0 && e == 0) break;
        memset(Mat,0,sizeof Mat);
        for(int i = 0;i < p; i ++) {
            scanf("%s %d %d",name[i] ,  &at[i] , & dmg[i]);
            Map[name[i]] = i;
        }
        for(int i = 0; i < v; i ++ ){
            scanf("%s %d %s", vl[i] ,  &def[i] , l);
            for(int i=0; l[i];i++) if(l[i]==',' )l[i]=' ';
            istringstream iss (l);
            string o;
            while(iss >> o ) {
                Mat[Map[o]][i] = 1;
            }
        }
        memset(dp,-1,sizeof dp);
        long long p = F(0,0);
        if(p <= e) printf("Yes");
        else printf("No");
        puts("");
        Map.clear();
                                      
    }
}