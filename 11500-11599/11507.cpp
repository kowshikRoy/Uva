//
//  main.cpp
//  11507 - Bender B. Rodríguez Problem
//
//  Created by Repon Macbook on 4/12/15.
//  Copyright (c) 2015 Repon Macbook. All rights reserved.
//
#include<cstdio>
#include<iostream>
#include<map>

using namespace std;

long L;
string dir, wire;
map<string, map<string, string> > bends;

int main() {
	bends["+x"]["+y"] = "+y";
	bends["+x"]["-y"] = "-y";
	bends["+x"]["+z"] = "+z";
	bends["+x"]["-z"] = "-z";
	bends["-x"]["+y"] = "-y";
	bends["-x"]["-y"] = "+y";
	bends["-x"]["+z"] = "-z";
	bends["-x"]["-z"] = "+z";
	bends["+y"]["+y"] = "-x";
	bends["+y"]["-y"] = "+x";
	bends["+y"]["+z"] = "+y";
	bends["+y"]["-z"] = "+y";
	bends["-y"]["+y"] = "+x";
	bends["-y"]["-y"] = "-x";
	bends["-y"]["+z"] = "-y";
	bends["-y"]["-z"] = "-y";
	bends["+z"]["+y"] = "+z";
	bends["+z"]["-y"] = "+z";
	bends["+z"]["+z"] = "-x";
	bends["+z"]["-z"] = "+x";
	bends["-z"]["+y"] = "-z";
	bends["-z"]["-y"] = "-z";
	bends["-z"]["+z"] = "+x";
	bends["-z"]["-z"] = "-x";
	
	for(;;) {
		scanf("%d", &L);
		if(L == 0) break;
		L--;
		wire = "+x";
		for(long i = 0; i < L; i++) {
			cin >> dir;
			if(dir != "No")
				wire = bends[wire][dir];
		}
		cout << wire << endl;
	}
}