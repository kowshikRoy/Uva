#include <bits/stdc++.h>
using namespace std;

const int N = 1e4+1;
vector<int>G[N];
bitset<N>B;
bool vis[N];
void dfs(int u)
{
    if(vis[u]) return;
    B[u] = 1;
    vis[u] = 1;
    for(int i = 0; i < G[u].size(); i ++ ) {
        int v = G[u][i];
        dfs(v);
    }
}
int main()
{
    int t;
    scanf("%d",&t);
    while(t--) {
        int n,m,l;
        scanf("%d %d %d",&n,&m,&l);
        for(int i = 0; i < m ; i ++) {
            int a,b;
            scanf("%d %d",&a,&b);
            G[a].push_back(b);
        }
        memset(vis,0,sizeof vis);
        B.reset();
        for(int i = 0; i < l ; i ++ ) {
            int p;
            scanf("%d",&p);
            dfs(p);
        }
        printf("%ld\n",B.count());
        for(int i = 1;i <= n; i ++) G[i].clear();
    }
    
}