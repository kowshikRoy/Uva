//
//  main.c
//  11567 - Moliu Number Generator
//
//  Created by MAC on 11/24/13.
//  Copyright (c) 2013 MAC. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[])
{

    long long n,res;
    
    while (scanf("%lld",&n)==1) {
        res = 0;
        while (n) {
            if (n==3) {
                res +=3;
                break;
            }
            if (n%2==0) {
                n = n/2;
            }
            else {
                if ((n+1)%4==0) {
                    n++;
                }
                else if ((n-1)%4==0) n--;
            }
            res++;
        }
        
        printf("%lld\n",res);
    }
    return 0;
}

