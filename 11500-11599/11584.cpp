#include<bits/stdc++.h>
using namespace std;
/*------- Constants---- */
#define forn(i,n)               for( int i=0 ; i < n ; i++ )
#define mp                      make_pair(i,j)
#define pb                      push_back((a))
#define all(x)                  (x).begin(),(x).end()
#define PI                      acos(-1.0)
#define EPS                     1e-9
#define xx                      first
#define yy                      second
#define lc                      ((n)<<1)
#define rc                      ((n)<<1|1)
#define db(x)                   cout << #x << " -> " << x << endl;
#define ms(ara_name,value)      memset(ara_name,value,sizeof(ara_name))

/*************************** END OF TEMPLATE ****************************/

const int N = 1005;
char s[N];
bool isP[N][N];
int dp[N];
int main()
{
    //freopen("/Users/MyMac/Desktop/in.txt","r",stdin);
    int t;
    cin >> t;
    while(t--) {
        scanf("%s",s);
        int n = strlen(s);
        for(int i = 0; i < n ;i ++ )isP[i][i]=1;
        for(int i = 0; i <n-1 ;i ++ ) {
            if(s[i]==s[i+1]) isP[i][i+1] = 1;
            else isP[i][i+1] = 0;
        }
        for(int len = 3; len <= n;  len ++ ) {
            for(int i = 0; i + len -1 < n ; i ++ ) {
                int j = i + len-1;
                if(s[i]== s[j]) isP[i][j] = isP[i+1][j-1];
                else isP[i][j] = 0;
            }
        }
        dp[0] = 1;
        for(int i = 1; i < n ;i ++) {
            dp[i] = 1e9;
            for(int j = 0; j <= i ; j ++ ) {
                if(isP[j][i]) dp[i] = min(dp[i] , 1 + (j>0?dp[j-1] : 0));
            }
        }
        printf("%d\n",dp[n-1]);
    }
    return 0;
}
/*
 3
 racecar
 fastcar
 aaadbccb
*/