#include <bits/stdc++.h>
using namespace std;
const int N = 1<<20;
struct PT {
    double  x,y;
    PT(double x=0, double y=0) : x(x), y(y) {}
    PT(const PT &p) : x(p.x), y(p.y)    {}
    PT operator + (const PT &p) const { return PT(x+p.x , y + p.y);}
    PT operator - (const PT &p) const { return PT(x-p.x , y - p.y);}
    PT operator * (double c) const { return PT(x*c,y*c);}
    PT operator / (double c) const { return PT(x/c,y/c);}
    void input(){scanf("%lf %lf",&x,&y);}
}p[N];

double dot(PT p,PT q){ return p.x * q.x + p.y * q.y;}
double cross(PT p,PT q) {return p.x*q.y - p.y*q.x;}
double dist2(PT p,PT q) {return dot(p-q , p-q);}
double DIM(PT p) {return sqrt(p.x*p.x+p.y*p.y);}
#define PI 3.1415926535897932
#define EPS 1e-8
double calc(PT a,PT b)
{
    double c = cross(a,b);
    if(c >= 0){
        double oo = dot(a,b) / DIM(a) / DIM(b);
        if(oo>1) oo=1;
        else if(oo<-1) oo= -1;
        return acos(oo);
    }
    else {
        double oo = dot(a,b) / DIM(a) / DIM(b);
        if(oo>1) oo=1;
        else if(oo<-1) oo= -1;
        return 2*PI - acos(oo);
    }
}
int main()
{
    int n;
    while(scanf("%d",&n)==1 && n) {
        for(int i = 0; i < n ; i ++) scanf("%lf %lf",&p[i].x, &p[i].y);
        double ang = 0;
        PT dir = p[1]-p[0];
        for(int i = 1; i < n ; i ++) {
            PT newdir = p[(i+1) %n] - p[i];
            ang += calc(dir, newdir);
            dir = newdir;
        }
        ang += calc(dir, p[1] - p[0]);
        printf("%.0lf\n", (ang / (2*PI)));
    }
}

