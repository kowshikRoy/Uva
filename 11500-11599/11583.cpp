#include <bits/stdc++.h>
using namespace std;

char s[10005];
int main()
{
	
	int t,n,cnt;
	scanf("%d",&t);
	while(t--) {
		scanf("%d",&n);
		int Mask = (1 << 26) -1, cnt = 0;
		for(int i = 0; i < n ;i  ++) {
			scanf("%s",s);
			int P= 0;
			for(int j = 0 ; s[j] ; j ++ ) {
				P |= (1 << (s[j]-'a'));
			}
			if(Mask & P ) {
				Mask = Mask & P;
			}
			else {
				cnt ++;
				Mask = P;
			}

		}
		cout << cnt << endl;
	}

}