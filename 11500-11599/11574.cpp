#include <bits/stdc++.h>
using namespace std;
#define PI acos(-1.0)
double r;
struct PT
{
    double x,y,ang,v;
    double dx,dy;
}p[1005];

double func(int i, int j , double T)
{
    double x1 = p[i].x  + p[i].dx * T * p[i].v , x2 = p[j].x + p[j].dx * T * p[j].v;
    double y1 = p[i].y  + p[i].dy * T * p[i].v , y2 = p[j].y + p[j].dy * T * p[j].v;
    double dx = x1-x2, dy = y1-y2;
    return sqrt(dx*dx + dy*dy) ;
}

void solve(int i,int j,double &dist, bool &flag)
{
    double low = 0 , high = 1000, ans = 1e19;
    flag = 0;
    while(high - low > 1e-2) {
        double f = low + (high - low ) /3;
        double g = high - (high - low ) /3;
        double ff = func(i,j,f) , fg = func(i,j,g);
        if(ff <= fg) {
            ans = f;
            high = g;
        }
        else low = f;
    }
    if(func(i,j,ans) <= r) {
        flag = 1;
        double low = 0, high = ans , ans2 = 1e9;
        while(high-low > 1e-2){
            double mid = (low + high) / 2;
            double k = func(i,j,mid);
            if(k <= r) {
                ans2 = mid ;
                high = mid;
            }
            else low = mid;
        }
        dist = ans2;
    }
}
int main()
{
    int t,n;
    scanf("%d",&t);
    while(t--) {
        scanf("%d %lf",&n,&r);
        for(int i = 0; i < n ;i ++ ) {
            scanf("%lf %lf %lf %lf",&p[i].y,&p[i].x,&p[i].ang, &p[i].v);
            p[i].ang *= PI/180;
            p[i]. dx = cos(p[i].ang);
            p[i]. dy = sin(p[i].ang);
        }
        double ans = 1e18;
        bool found  = 0;
        for(int i = 0; i < n; i ++) {
            for(int j = i+1; j < n ; j ++ ) {
                double d = 1e9;
                bool f;
                solve(i,j,d,f);
                if(f) {
                    ans = min(ans, d);
                    found = 1;
                }
            }
        }
        if(found ) printf("%d\n",(int) (ans + .5 ));
        else printf("No collision.\n");
        
    }
}