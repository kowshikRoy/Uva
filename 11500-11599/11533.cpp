#include <bits/stdc++.h>
using namespace std;

bool vis[260*260];
int main()
{
    int cs = 0;
    long long n,x,y;
    while(scanf("%lld %lld %lld",&n,&x,&y)==3) {
        printf("Case %d:",++cs);
        if(x==0 && y == 0) {
            printf(" 0\n");
            continue;
        }
        if(y >= n || x>=n) {
            printf(" No solution\n");
            continue;
        }
        vector<long long>v;
        v.push_back(x);
        long long prv = x;
        long long carry = 0;
        bool flag = 0;
        memset(vis,0,sizeof vis);
        while(1) {
            long long now = prv * y + carry;
            long long dig = now % n;
            carry = now / n;
            
            if(dig == x && carry == 0  ) {
                flag = 1;
                break;
            }
            v.push_back(dig);
            if(vis[now]) break;
            vis[now] = 1;
            prv = dig;
        }
        if(flag) {
            reverse(v.begin(), v.end());
            for(auto a: v) printf(" %lld",a);
        }
        else {
            printf(" No solution");
        }
        printf("\n");
        v.clear();
    }
}