#include <bits/stdc++.h>
using namespace std;
#define MP make_pair
vector<int>v;
bool w[26];
int n,CS = 1;
string s;
int vis[30][30];
pair<int,long long> dp[30][30];
pair<int,long long> calc(int i,int prv)
{
    
    if(i==n) return dp[i][prv] = MP(0,1);
    if(vis[i][prv] == CS) return dp[i][prv];
    pair<int,long long> res, res2;
    res = MP(-1,1);
    res2 = MP(0,1);
    if(5 * prv <= 4 * v[i]) {
        auto p = calc(i+1,v[i]);
        res = MP(p.first + 1, p.second);
    }
    res2 = calc(i+1,prv);
    if(res.first != res2.first)  {
        dp[i][prv] = (res.first > res2.first) ? res : res2;
    }
    else dp[i][prv] = MP(res.first , res.second + res2.second);
    vis[i][prv] = CS;
    return dp[i][prv];
}
int main()
{
    int t;
    scanf(" %d ",&t);
    while(t--) {
        getline(cin,s);
        memset(w,0,sizeof w);
        v.clear();
        for(int i = 0; s[i] ; i ++)  {
            if(isupper(s[i]))w[s[i]-'A'] = 1;
        }
        for(int i = 0; i < 26; i ++ ) if(w[i]) v.push_back(i+1);
        n = v.size();
        pair<int,long long > o = calc(0,0);
        printf("%d %lld\n",o.first, o.second);
        CS ++;
    }
}