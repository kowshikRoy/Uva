#include <bits/stdc++.h>
using namespace std;
const int N = 1e6+6;
long long dp[N];
long long func(long long p)
{
    return p * (p+1)/2;
}
int main()
{
    for(long long i = 3; i <= N; i ++ ) {
        long long k = i / 2;
        if(k*2<=i) k++;
        long long sum = 2 * (func(i-1) - func(k-1)) - (i-1-k+1) - i * (i-1-k+1);
        dp[i] = dp[i-1] + sum;
    }
    int t;
    scanf("%d",&t);
    while(t--) {
        int n;
        scanf("%d",&n);
        printf("%lld\n",dp[n]);
    }
}