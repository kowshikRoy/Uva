#include <bits/stdc++.h>
using namespace std;
#define MP make_pair
int r,c, x , y;
char grid[55][55];
bool vis[55][55], draft[55][55];
int dir[][2] = {{0,1},{0,-1},{1,0},{-1,0}};
bool ok(pair<int,int> a)
{
    if(a.first >= 0 && a.first < r && a.second >= 0 && a.second < c && grid[a.first][a.second] != '#' && grid[a.first][a.second] != 'T') return 1;
    return 0;
}
void solve()
{
    memset(draft,0,sizeof draft);
    for(int i = 0; i < r; i ++ ) {
        for(int j = 0; j < c; j ++)  {
            if(grid[i][j] == 'T') {
                for(int k = 0; k < 4; k ++ ) {
                    int xp = i + dir[k][0];
                    int yp = j + dir[k][1];
                    if(ok(MP(xp,yp))) draft[xp][yp] = 1;
                }
            }
        }
    }
    if(draft[x][y]) {printf("0\n");return;}
    memset(vis,0,sizeof vis);
    vis[x][y] =1;
    queue<pair<int,int> > q;
    q.push(MP(x,y));
    while(!q.empty()){
        auto a = q.front(); q.pop();
        for(int i = 0; i < 4; i ++ ) {
            auto b = MP(a.first + dir[i][0] , a.second + dir[i][1]);
            if(ok(b) && vis[b.first][b.second] == 0 ) {
                vis[b.first] [b.second] = 1;
                if(draft[b.first][b.second] == 0)q.push(b);
            }
        }
    }
    int cnt = 0;
    for(int i= 0;i < r; i ++)
        for(int j = 0; j < c; j ++ ) {
            if(vis[i][j] && grid[i][j] == 'G' ) cnt ++;
        }
    printf("%d\n",cnt);
}
int main()
{
    while(scanf("%d %d",&c,&r)==2) {
        for(int i =0 ; i < r;i ++) scanf("%s", grid[i]);
        for(int i = 0; i < r;i ++) {
            for(int j = 0; j < c; j ++ ) {
                if(grid[i][j] == 'P') {
                    x = i, y = j;
                    
                }
            }
        }
        solve();
    }
    
}
/*
7 4
#######
#P.GTG# 
#..TGG# 
#######
8 6
########
#...GTG# 
#..PG.G# 
#...G#G#
#..TG.G#
########

*/