#include <bits/stdc++.h>
using namespace std;

namespace gauss {
    double EPS = 1e-8;
    typedef double T;
    typedef vector<T> VT;
    typedef vector<VT> VVT;
    
    
    int Gauss(VVT &a)
    {
        int n = a.size(), m= a[0].size();
        int r = 0;
        for(int c = 0; c < m - 1 && r < n ; c ++ ) {
            int j = r;
            for(int i = r+1; i < n; i ++ ) {
                if(fabs(a[i][c]) > fabs(a[j][c])) j= i;
            }
            if(fabs(a[j][c]) < EPS) continue;
            swap(a[j],a[r]);
            T s = 1./a[r][c];
            for(int i = 0; i < m ; i ++ ) a[r][i] *= s;
            for(int i=0;i < n ;i ++) if(i!=r) {
                T t = a[i][c];
                for(int j = 0; j < m ; j ++ ) a[i][j] -= t * a[r][j];
            }
            r++;
        }
        return r;
        
    }
}
using namespace gauss;

int main()
{
    int a,b,l,w;
    while(scanf("%d %d %d %d",&a,&b,&l,&w)==4 ) {
        if(a+b+l+w == 0) break;
        map<pair<int,int> ,int>Map;
        int cnt = 0;
        for(int i = 0; ; i ++ ) {
            Map[make_pair(a+i*w, max(0,b-i*w))] =cnt ++;
            if(b-i * w <= 0 ) break;
        }
        for(int i = 1; ; i ++ ) {
            Map[make_pair(max( a - i*w , 0), b + i * w)] =cnt ++;
            if(a-i * w <= 0 ) break;
        }
        int Dim = Map.size();
        VVT A ( Map.size() , VT ( Map.size() + 1, 0) );
        for(auto a: Map ) {
            pair<int,int> P = a.first ;
            if(P.first == 0 || P.second == 0 ) {
                A[Map[P]][Map[P]] = 1;
                A[Map[P]][Dim] = (P.first ? 1 : 0);
                continue;
            }
            pair<int,int> F = make_pair(P.first + w , max(0, P.second - w));
            pair<int,int> S = make_pair(max(P.first - w ,0 ) , P.second + w);
            A[Map[P]][Map[P]] = 1;
            A[Map[P]][Map[F]] = -1. * l / 6;
            A[Map[P]][Map[S]] = -1. * (6-l) / 6;
            A[Map[P]][Dim]  = 0;
        }
        Gauss(A);
        
        printf("%.1lf\n",A[Map[make_pair(a,b)]][Dim] * 100);
    }
}
