//
//  main.cpp
//  11525 - Permutation
//
//  Created by Repon Macbook on 3/30/15.
//  Copyright (c) 2015 Repon Macbook. All rights reserved.
//

#include <bits/stdc++.h>
using namespace std;

/*------- Constants---- */
#define LMT                     100006
#define ll                      long long
#define ull                     unsigned long long
#define mod                     1000000007
#define MEMSET_INF              63
#define MEM_VAL                 1061109567
#define FOR(i,n)                for( int i=0 ; i < n ; i++ )
#define mp(i,j)                 make_pair(i,j)
#define lop(i,a,b)              for( int i = (a) ; i < (b) ; i++)
#define pb(a)                   push_back((a))
#define gc                      getchar_unlocked
#define PI                      acos(-1.0)
#define inf                     1<<30
#define lc                      ((n)<<1)
#define rc                      ((n)<<1 |1)

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;
/*------ template functions ------ */
inline void sc(int &x)
{
	register int c = gc();
	x = 0;
	int neg = 0;
	for(;((c<48 | c>57) && c != '-');c = gc());
	if(c=='-') {neg=1;c=gc();}
	for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
	if(neg) x=-x;
}

template <class T> inline T bigmod(T p,T e,T M){
	ll ret = 1;
	for(; e > 0; e >>= 1){
		if(e & 1) ret = (ret * p) % M;
		p = (p * p) % M;
	} return (T)ret;
}
template <class T> inline T gcd(T a,T b){if(b==0)return a;return gcd(b,a%b);}
template <class T> inline T modinverse(T a,T M){return bigmod(a,M-2,M);}

/*************************** END OF TEMPLATE ****************************/

int mVal;
int A[LMT];
int ibit[LMT];

void updt(int pos,int val)
{
	while (pos<= mVal) {
		ibit[pos] += val;
		pos = pos + (pos &-pos);
		
	}
}

int query(int pos)

{
	int sum = 0;
	while( pos > 0 ) {
		sum += ibit[pos];
		pos -= (pos &-pos);
	}
	return pos;
}

int bs(int val)
{
	int idx = 0 , tmpx = 0;
	int gap = mVal ;
	while( idx <= mVal && gap > 0 ){
		tmpx = idx + gap ;
		if( val > ibit[tmpx] ) {
			idx = tmpx;
			val -= ibit[tmpx];
		}
		gap/=2;
	}
	return idx + 1 ;
}
vi kp;
int main()
{
	
	int tc , n ;
	sc(tc);
	while (tc -- ) {
		sc(n);
		mVal = ((n&(n-1)) ==0 ? n : 1<<(int)(log2(n) + 1));
		for(int i = 1; i <= n ; i ++ ) {
			sc(A[i]) ;
			updt(i,1);
		}
		for(int i = 1 ; i <= n ; i ++ ) {
			//
			int k = bs(A[i] + 1);
			updt(k , -1);
			kp.push_back(k);
			
		}
		for(int i =0 ; i < kp.size() ; i ++ ) {
			if(i) printf(" ");
			printf("%d",kp[i]);
		}
		//ms(ibit, 0);
		printf("\n");
		kp.clear();
	}
	return 0;
}