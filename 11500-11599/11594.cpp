/*
 
 *************************
 Id  : Matrix.code
 Task:
 Date:
 
 **************************
 
 */

#include <bits/stdc++.h>
using namespace std;

/*------- Constants---- */

#define Long                    long long
#define ull                     unsigned long long
#define mod                     1000000007
#define MEMSET_INF              63
#define MEM_VAL                 1061109567
#define forn(i,n)               for( int i=0 ; i < n ; i++ )
#define mp(i,j)                 make_pair(i,j)
#define lop(i,a,b)              for( int i = (a) ; i < (b) ; i++)
#define pb(a)                   push_back((a))
#define all(x)                  (x).begin(),(x).end()
#define gc                      getchar_unlocked
#define PI                      acos(-1.0)
#define INF                     1<<29
#define EPS                     1e-9
#define Fr                      first
#define Sc                      second
#define Sz                      size()
#define lc                      ((n)<<1)
#define rc                      ((n)<<1|1)
#define db(x)                   cout << #x << " -> " << x << endl;
#define Di(n)                   int n;si(n)
#define Dii(a,b)                int a,b;si(a);si(b)
#define Diii(a,b,c)             int a,b,c;si(a);si(b);si(c)
#define Si(n)                   si(n)
#define Sii(a,b)                si(a);si(b)
#define Siii(a,b,c)             si(a);si(b);si(c)
#define min(a,b)                ((a)>(b) ? (b) : (a) )
#define max(a,b)                ((a)>(b) ? (a):(b))
/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;
typedef vector<Long> vl;
/*------ template functions ------ */
#ifndef getchar_unlocked
#define getchar_unlocked getchar
#endif
template<class T> inline void si(T &x){register int c = gc();x = 0;int neg = 0;for(;((c<48 | c>57) && c != '-');c = gc());
      if(c=='-') {neg=1;c=gc();}for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}if(neg) x=-x;}
template <class T> inline T bigmod(T p,T e,T M){Long ret = 1;for(; e > 0; e >>= 1){if(e & 1) ret = (ret * p) % M;p = (p * p) % M;} return (T)ret;}
template <class T> inline T gcd(T a,T b){if(b==0)return a;return gcd(b,a%b);}
template <class T> inline T modinverse(T a,T M){return bigmod(a,M-2,M);}
void io(){freopen("/Users/MyMac/Desktop/in.txt","r",stdin);}

/*************************** END OF TEMPLATE ****************************/
const int N = 205;

struct Edge{
      int u,v;
      Long cap,flow;
};
struct Dinic{
      int n,m,s,t;
      vector<int>G[N];
      vector<Edge>E;
      int cur[N];
      int d[N];
      bool vis[N];
      
      void init(int n)
      {
            this->n=n;
            forn(i,n+1) G[i].clear();
            E.clear();
      }
      
      void addEdge(int u,int v,Long cap)
      {
            E.push_back({u,v,cap,0});
            E.push_back({v,u,0,0});
            m = E.Sz;
            G[u].pb(m-2);
            G[v].pb(m-1);
      }
      
      bool bfs()
      {
            ms(vis,0);
            queue<int>q;
            q.push(s);
            vis[s]=1;
            d[s] = 0;
            while(!q.empty()){
                  int u = q.front() ; q.pop();
                  for(int i = 0; i < G[u].Sz;i ++ ) {
                        Edge &e = E[G[u][i]];
                        if(vis[e.v] == 0 && e.cap > e.flow) {
                              vis[e.v]  =1;
                              d[e.v] = 1 + d[u];
                              q.push(e.v);
                        }
                  }
            }
            return vis[t];
      }
      
      Long dfs(int u,Long oo)
      {
            if(u==t || oo == 0) return oo;
            Long Flow = 0, f;
            for(int &i = cur[u] ; i < G[u].Sz ; i ++ ) {
                  Edge &e= E[G[u][i]];
                  if(d[e.v] == 1 + d[u] && (f = dfs(e.v , min(oo  , e.cap - e.flow))) > 0 ){
                        e.flow += f;
                        E[G[u][i] ^1 ].flow -=f;
                        Flow += f;
                        oo-=f;
                        if(oo == 0 ) break;
                  }
            }
            return Flow;
            
      }
      
      Long dinitz(int s,int t)
      {
            this->s= s, this->t=t;
            Long Mf = 0;
            while(bfs()){
                  ms(cur,0);
                  Mf += dfs(s,1LL<<45);
            }
            return Mf;
      }
      vector<int> getCut()
      {
            vector<int>cut;
            for(int i = 0;i < E.Sz; i ++ ) {
                  Edge &e = E[i];
                  if(e.flow > 0 && e.cap == e.flow ) cut.pb(i);
            }
            return cut;
      }
      
      void clear()
      {
            for(int i = 0;i < m ;i ++ ) E[i].flow = 0;
      }
      
      
}MaxF;

int F[N];
Long Ans[N][N];
int e[N];
vector<ii> T[N];
int id;

void dfs(int u,int p ,Long flow )
{
      
      for(auto a: T[u]) if(a.Fr != p) {
            Ans[id][a.Fr] = min(flow, a.Sc);
            dfs(a.Fr, u, min(flow  , a.Sc ));
      }
}
int main()
{
      Di(t);
      int cs = 0;
      while(t--) {
            Di(n);
            MaxF.init(n);
            forn(i,n ) forn(j,n) {
                  Di(a);
                  if(a)MaxF.addEdge(i,j,a);
            }
            
            forn(i,n) F[i] = 0;
            for(int i=1;i<n;i++ ){
                  MaxF.clear();
                  e[i] = MaxF.dinitz(i,F[i]);
                  MaxF.bfs();
                  for(int j = i+1; j< n; j ++ ) {
                        if(MaxF.vis[j] && F[j] == F[i] ) F[j]= i;
                  }
            }
            
            for(int i = 1; i< n; i ++ ) {
                  T[i].pb(mp(F[i],e[i]));
                  T[F[i]].pb(mp(i,e[i]));
            }
            forn(i,n) {
                  id = i;
                  dfs(i,-1, 1LL<<60 );
            }
            printf("Case #%d:\n",++cs);
            forn(i,n) {
                  forn(j,n){
                        if(j) printf(" ");
                        printf("%lld", Ans[i][j]);
                  }
                  printf("\n");
            }
            forn(i,n) T[i].clear();
            
      }
      return 0;
}