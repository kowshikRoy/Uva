//
//  main.cpp
//  11517 - Exact Change
//
//  Created by Repon Macbook on 4/19/15.
//  Copyright (c) 2015 Repon Macbook. All rights reserved.
//

#include <bits/stdc++.h>
using namespace std;

/*------- Constants---- */
#define LMT				105
#define ll				long long
#define ull				unsigned long long
#define mod				1000000007
#define MEMSET_INF		63
#define MEM_VAL			1061109567
#define FOR(i,n)			for( int i=0 ; i < n ; i++ )
#define mp(i,j)			make_pair(i,j)
#define lop(i,a,b)		for( int i = (a) ; i < (b) ; i++)
#define pb(a)			push_back((a))
#define gc				getchar_unlocked
#define PI				acos(-1.0)
#define inf				1<<30
#define lc				((n)<<1)
#define rc				((n)<<1 |1)
#define msg(x)			cout<<x<<endl;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;
/*------ template functions ------ */
inline void sc(int &x)
{
	register int c = gc();
	x = 0;
	int neg = 0;
	for(;((c<48 | c>57) && c != '-');c = gc());
	if(c=='-') {neg=1;c=gc();}
	for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
	if(neg) x=-x;
}

template <class T> inline T bigmod(T p,T e,T M){
	ll ret = 1;
	for(; e > 0; e >>= 1){
		if(e & 1) ret = (ret * p) % M;
		p = (p * p) % M;
	} return (T)ret;
}
template <class T> inline T gcd(T a,T b){if(b==0)return a;return gcd(b,a%b);}
template <class T> inline T modinverse(T a,T M){return bigmod(a,M-2,M);}

/*************************** END OF TEMPLATE ****************************/


int N ;

int dp[30005];
int coin[LMT];

int main()
{
	
	int tc , n ;
	sc(tc);
	while (tc -- ) {
		sc(n);
		sc(N);
		
		int iM = inf;
		FOR(i, N) {
			sc(coin[i]);
			if(coin[i] < iM ) iM = coin[i];
		}
		
		
		ms(dp, MEMSET_INF);
		dp[0] = 0;
		
		int lim = 30000 ;
		for(int i = 0 ; i < N ; i ++ ) {
			for(int sum = lim ; sum >= coin[i] ; sum -- ) {
				if(dp[sum - coin[i] ] <  MEM_VAL) {
					dp[sum] = min(dp[sum] , 1 + dp[sum - coin[i]] );
				}
			}
		}
		
		for(int i = n ; i <= lim ; i ++ ) {
			if(dp[i] < MEM_VAL) {
				lim = i ;
				break;
			}
		}
		printf("%d %d\n" , lim , dp[lim] );
		
		
	}
	
	return 0;
}