#include <bits/stdc++.h>
using namespace std;

int a[1005][26];
char s[1005];
int dp[1005][30];
int value[1005];
int k;
int len;
int solve(int ind, int prv)
{
    if(ind==len) return 0;
    if(dp[ind][prv] !=-1) return dp[ind][prv];
    int res = 1e8;
    for(int i = 0; i < 26; i ++ ) {
        if(a[ind][i]) {
            if(i==prv) {
                for(int j= 0; j < 26; j ++) {
                    if(i==j || a[ind][j] == 0) continue;
                    res = min(res , value[ind] -1 +  solve(ind+1, j ));
                }
                if(a[ind][i] > 1) {
                    res = min(res, value[ind] + solve(ind+1,i));
                }
                if(a[ind][i] > 1 && value[ind] == 1) {
                    res = min(res, solve(ind+1,i));
                }
                if(k==1) {
                    res = min(res, value[ind] -1 + solve(ind+1 , i));
                }
            }
            else {
                res = min(res, value[ind] + solve(ind +1 , i));
            }
        }
    }
    return dp[ind][prv] = res;
}

int main()
{
    //freopen("in.txt","r",stdin);
    int t,cs=0;
    scanf("%d",&t);
    while(t--) {
        scanf("%d %s",&k,s);
        int b[26];
        memset(b,0,sizeof b);
        memset(a,0,sizeof a);
        for(int i = 0; s[i] ; i ++ ) {
            b[s[i]-'a'] ++;
            if(i%k==k-1) {
                memcpy(a[i/k] , b , sizeof b);
                memset(b,0,sizeof b);
            }
            
        }
        
        len = strlen(s)/k;
        for(int i = 0; i < len; i ++ ) {
            int sum =0;
            for(int j = 0; j < 26; j ++ ) {
                sum += a[i][j] ? 1 : 0;
            }
            value[i] = sum;
        }
        memset(dp,-1,sizeof dp);
        printf("%d\n",solve(0,26));
    }
}