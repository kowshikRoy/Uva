//
//  main.cpp
//  11520 - Fill the Square
//
//  Created by Repon Macbook on 6/14/15.
//  Copyright (c) 2015 MAC. All rights reserved.
//
#include <bits/stdc++.h>
using namespace std;

/*------- Constants---- */
#define LMT				15
#define ll				long long
#define ull				unsigned long long
#define mod				1000000007
#define MEMSET_INF		63
#define MEM_VAL			1061109567
#define FOR(i,n)			for( int i=0 ; i < n ; i++ )
#define mp(i,j)			make_pair(i,j)
#define lop(i,a,b)		for( int i = (a) ; i < (b) ; i++)
#define pb(a)			push_back((a))
#define gc				getchar_unlocked
#define PI				acos(-1.0)
#define inf				1<<30
#define lc				((n)<<1)
#define rc				((n)<<1 |1)
#define debug(x)              cout <<#x <<" -> " << x << endl;
#define EPS				1e-7
#define fred()                 freopen("in.txt","r",stdin);
#define fwrt()               freopen("in.txt","w",stdout);
/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;
/*------ template functions ------ */
inline void sc(int &x)
{
	register int c = gc();
	x = 0;
	int neg = 0;
	for(;((c<48 | c>57) && c != '-');c = gc());
	if(c=='-') {neg=1;c=gc();}
	for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
	if(neg) x=-x;
}

template <class T> inline T bigmod(T p,T e,T M){
	ll ret = 1;
	for(; e > 0; e >>= 1){
		if(e & 1) ret = (ret * p) % M;
		p = (p * p) % M;
	} return (T)ret;
}
template <class T> inline T gcd(T a,T b){if(b==0)return a;return gcd(b,a%b);}
template <class T> inline T modinverse(T a,T M){return bigmod(a,M-2,M);}

/*************************** END OF TEMPLATE ****************************/

string grid[LMT];
int n ;

bool valid( int x, int y)
{
      if( x < 0 || y < 0 || x >= n || y >= n ) return false;
      return true;
}

int dir[][2] = {{0,1},{0,-1},{-1,0},{1,0}};
int main()
{
      
      int tc ,cs = 0 ;
      sc(tc);
      while (tc -- ) {
            sc(n);
            for( int i = 0; i < n ; i ++ ) {
                  cin >> grid[i];
            }
            
            for(int i =0 ; i < n ; i ++ ) {
                  for (int j = 0 ; j < n ; j ++ ) {
                        if(grid[i][j] !='.') continue;
                        for( int p = 0 ; p < 26 ;p ++ ) {
                              bool flag = true;
                              for( int d = 0 ; d < 4 ; d ++ ) {
                                    int x = i + dir[d][0];
                                    int y = j + dir[d][1];
                                    
                                    if(valid(x,y) && grid[x][y] == p + 'A' ) {
                                          flag = false;
                                    }
                              }
                              
                              if( flag ) {
                                    grid[i][j] = p +'A';
                                    break;
                              }
                              
                        }
                  }
            }
            
            printf("Case %d:\n" , ++cs) ;
            for( int i = 0 ;i < n ; i ++) {
                  for ( int j = 0; j < n ; j ++ ) {
                        printf("%c",grid[i][j] );
                  }
                  printf("\n");
            }
            
      }
      
      return 0;
}