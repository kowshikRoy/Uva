#include <bits/stdc++.h>
using namespace std;

int g[31][50005];
map<vector<int>,int>Map;
int main()
{
    int l,k,t,cs=0;
    while(scanf("%d %d",&l,&k)==2){
        if(l==0&&k==0) break;
        memset(g,0,sizeof g);
        for(int i = 0; i < k; i ++) {
            scanf("%d",&t);
            while(t--){
                int o;
                scanf("%d",&o);
                g[i][o+1] = 1;
            }
            for(int j= 0; j <= l; j ++) {
                g[i][j] = g[i][j] + g[i][j-1];
            }
        }
        if(k==1) {
            printf("Case #%d: %d meter(s)\n",++cs, l);
            continue;
        }
        
        int ans = 0;
        for(int i = 0; i <= l; i ++ ) {
            vector<int>v;
            for(int j = 0; j + 1 < k; j ++ ) {
                v.push_back(g[j][i] - g[j+1][i]);
            }
            if(Map.count(v)) {
                ans = max(ans , i - Map[v]  );
            }
            else Map[v] = i;
        }
        Map.clear();
        printf("Case #%d: %d meter(s)\n",++cs, ans);
    }
}
/*
5 2
2 1 3
1 0
5 3
3 0 3 4
2 0 3
2 3 4
*/