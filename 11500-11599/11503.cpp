//
//  main.cpp
//  11503 - Virtual Friends
//
//  Created by utpal roy on 26/08/2014.
//  Copyright (c) 2014 utpal roy. All rights reserved.
//

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<map>
#include<queue>
#include<stack>
#include<cstdlib>
#include<algorithm>
#include<set>
#include<iterator>
#include<cassert>
#include <sstream>
using namespace std;

/*------- Constants---- */
#define MX 100005
#define ll long long
#define ull unsigned long long
#define mod 1000000007
#define pb push_back
#define pop pop_back

/* -------Global Variables ------ */
ll x,y,d;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))


/*------ template functions ------ */
template < class T > inline T gcd(T a , T b ) { if(b==0) return a; else return gcd(b, a%b);}
template < class T > inline T lcm(T a , T b ) { return  a*b / gcd(a, b);}
template < class T > inline T extended_euclid_returning_gcd(T a,T b){ T t; if(b==0){d = a;x=1;y=0;} else {extended_euclid_returning_gcd(b, a%b); t=x; x=y;y = t-(a*y/b);}}
template < class T > inline T absolute(T a ) { if(a>0) return a; else return -a;}
template < class T > inline T reverse_num( T a ){ T x=0,tmp = a; while(tmp) { x=10*x+ tmp % 10; tmp/=10;} return x;}
template <class T > inline T big_mod(T base, T power){ T res=1; while (power) { if(power&1){ res*=base; power--;} base =base *base; power/=2;} return res;}

//freopen("//Users//utpalroy//Desktop//in.txt", "w", stdout);

string s1,s2;
map<string, int> kl;
int para[MX];
int size[MX];
int find_represent(int a)
{
    if(para[a]==a) return a;
    else return para[a] = find_represent(para[a]);
}

int friendship(int a,int b)
{
    int u = find_represent(a);
    int v = find_represent(b);
    
    if(u!=v){
        para[u]=v;
        size[v] = size[v] + size[u];
    }
    return size[v];
}

int main()
{
    int test,f,inx;
    cin>>test;
    
    while (test--) {
        cin>>f;
        inx=0;
        for(int i=0;i<MX;i++) {para[i]=i;size[i]=1;}
        while (f--) {
            cin>>s1>>s2;
            if (kl.count(s1)==0) {
                kl[s1]=inx++;
            }
            if(kl.count(s2)==0) kl[s2]=inx++;
            
            cout<<friendship(kl[s1],kl[s2])<<"\n";
        }
        kl.clear();
        
    }
    return 0;
}


