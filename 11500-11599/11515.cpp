#include <bits/stdc++.h>
using namespace std;
const int N = 15;
int x[N],y[N],r[N];

bool iN(int i,int j)
{
    long long dx = x[i] - x[j], dy = y[i] - y[j];
    if(dx*dx + dy*dy <= (r[i] + r[j]) * (r[i] + r[j])) return 1;
    return 0;
}
int main()
{
    int t;
    scanf("%d",&t);
    while(t--) {
        int n;
        scanf("%d",&n);
        for(int i = 0; i < n ;i ++) scanf("%d %d %d",&x[i],&y[i], &r[i]);
        long long ans = 0;
        for(int i = 0; i < 1 << n; i ++ ) {
            vector<int>v;
            long long can = 0;
            for(int j = 0; j < n ; j ++ ) {
                if(i&1<<j) {
                    v.push_back(j);
                    can += r[j] * r[j];
                }
            }
            bool flag= 1;
            for(int j = 0; j < v.size() ; j ++ ) {
                for(int k = j + 1; k < v.size(); k ++ ) {
                    if(iN(v[j],v[k])){
                        flag = 0;
                        break;
                    }
                }
            }
            if(flag) ans = max(ans, can);
        }
        printf("%lld\n",ans);
    }
}