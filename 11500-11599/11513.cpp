#include <bits/stdc++.h>
using namespace std;

map<string,int> Map;
map<int,string> RevMap;
map<int,int> dp;
int a[3][3];
int cnt  = 0;
string encode(int a[][3])
{
    string s;
    for(int i = 0; i < 3; i ++) {
        for(int j = 0; j < 3; j ++ ) s.push_back(a[i][j] + '0');
    }
    return s;
}
void decode(int u,int a[][3])
{
    string s = RevMap[u];
    for(int i = 0, k = 0; i < 3; i ++ )
        for(int j = 0; j < 3; j ++ ){
            a[i][j] = s[k] - '0';
            k++;
        }
    return;
}


int get(string s)
{
    if(Map.count(s)) return Map[s];
    Map[s] = ++cnt;
    RevMap[cnt] = s;
    return cnt;
}

void HR(int a[][3], int row, int b[][3])
{
    for(int i=0;i<3;i++)for(int j=0;j<3;j++)b[i][j]=a[i][j];
    for(int j = 1; j < 3; j ++ ) b[row][j-1]= a[row][j];
    b[row][2] = a[row][0];
}

void VR(int a[][3], int col, int b[][3])
{
    for(int i=0;i<3;i++)for(int j=0;j<3;j++)b[i][j]=a[i][j];
    for(int i = 1; i < 3; i ++) b[i][col] = a[i-1][col];
    b[0][col] = a[2][col];
}

map<int,pair<int,int> > Par;
map<int,int> F;
void solve()
{
    string s = encode(a);
    int u = get(s);
    queue<int>q;
    q.push(u);
    dp[u] = 0;
    int b[3][3];
    while(!q.empty()){
        int u = q.front(); q.pop();
        decode(u,a);
        for(int i = 0; i < 3; i ++ ) {
            HR(a,i,b);
            string vs = encode(b);
            int v = get(vs);
            if(dp.count(v) == 0 || dp[v] > dp[u] + 1 ) {
                dp[v] = 1 + dp[u];
                Par[v] = make_pair(0,i);
                q.push(v);
                F[v] = u;
            }
        }
        for(int i = 0; i < 3; i ++ ) {
            VR(a,i,b);
            string vs = encode(b);
            int v = get(vs);
            if(dp.count(v) == 0 || dp[v] > dp[u] + 1 ) {
                dp[v] = 1 + dp[u];
                Par[v] = make_pair(1,i);
                F[v] = u;
                q.push(v);
            }
        }
        
    }
}
int main()
{
    int o = 1;
    for(int i = 0; i < 3; i ++) for(int j = 0; j < 3; j ++ ) a[i][j] = o , o++;
    solve();
    int n;
    while(scanf("%d",&n) == 1 && n) {
        a[0][0] = n;
        scanf("%d %d",&a[0][1], &a[0][2]);
        for(int i = 1; i < 3;i ++ )for(int j= 0;j<3;j++) scanf("%d",&a[i][j]);
        
        string s = encode(a);
        if(Map.count(s)) {
            int u = Map[s];
            vector<pair<int,int> > path;
            while(u != 1) {
                path.push_back(Par[u]);
                u = F[u];
            }
            printf("%d ", (int)path.size());
            for(int i = 0; i < path.size(); i ++ ) {
                printf("%c%d",path[i].first==0 ? 'H' :'V', path[i].second +1);
            }
            printf("\n");
        }
        else printf("Not solvable\n");
        
    }
}

