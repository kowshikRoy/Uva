//
//  main.cpp
//  11549 - Calculator Conundrum
//
//  Created by utpal roy on 19/08/2014.
//  Copyright (c) 2014 utpal roy. All rights reserved.
//

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<map>
#include<queue>
#include<stack>
#include<cstdlib>
#include<algorithm>
#include<set>
#include<iterator>
#include<cassert>
#include <sstream>
using namespace std;

/*------- Constants---- */
#define MX 100
#define ll long long
#define ull unsigned long long
#define mod 1000000007

/* -------Global Variables ------ */
ll x,y,d;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))


/*------ template functions ------ */
template < class T > inline T gcd(T a , T b ) { if(b==0) return a; else return gcd(b, a%b);}
template < class T > inline T lcm(T a , T b ) { return  a*b / gcd(a, b);}
template < class T > inline T extended_euclid_returning_gcd(T a,T b){ T t; if(b==0){d = a;x=1;y=0;} else {extended_euclid_returning_gcd(b, a%b); t=x; x=y;y = t-(a*y/b);}}
template < class T > inline T absolute(T a ) { if(a>0) return a; else return -a;}
template < class T > inline T reverse_num( T a ){ T x=0,tmp = a; while(tmp) { x=10*x+ tmp % 10; tmp/=10;} return x;}
template <class T > inline T big_mod(T base, T power){ T res=1; while (power) { if(power&1){ res*=base; power--;} base =base *base; power/=2;} return res;}


set<ll> mango;
int main()
{
	ll test,n,k,digit;
    cin>>test;
    set<ll>::iterator it;
    while (test--) {
        cin>>n>>k;
        mango.insert(k);
        while (1) {
            k=k*k;
            digit = log10(k)+1;
            if(digit>n){
                k = k/pow(10, digit-n);
            }
            if(mango.find(k)!=mango.end()) break;
            mango.insert(k);
            
            
        }
        it =-- mango.end();
        printf("%lld\n",*it);
        mango.clear();
        
    }
    return 0;
}
