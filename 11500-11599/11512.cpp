//
//  main.cpp
//  11512 - GATTACA
//
//  Created by Repon Macbook on 12/9/14.
//  Copyright (c) 2014 Repon Macbook. All rights reserved.
//


#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<queue>
#include<stack>
#include<cstdlib>
#include<algorithm>
#include<set>
#include<iterator>
#include<cassert>
#include <sstream>
#include <climits>
#include <list>
#include <string>
using namespace std;

/*------- Constants---- */
#define MX 1007
#define ll long long
#define ull unsigned long long
#define mod 1000000007
#define MEMSET_INF 63
#define MEM_VAL 1061109567
#define FOR(i,n) for( int i=0 ; i < n ; i++ )
#define REP(i, n) for (int i = 0; i < (int)(n); ++i)
#define mp(i,j) make_pair(i,j)
#define lop(i,a,b) for( int i = (a) ; i < (b) ; i++)
#define pb(a) push_back((a))
#define gc getchar_unlocked
/* -------Global Variables ------ */
ll euclid_x,euclid_y,d;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;
/*------ template functions ------ */
template < class T > inline T gcd(T a , T b ) { if(b==0) return a; else return gcd(b, a%b);}
template < class T > inline T lcm(T a , T b ) { return  a*b / gcd(a, b);}
template < class T > inline T extended_euclid_returning_gcd(T a,T b){ if(b==0){d = a;euclid_x=1;euclid_y=0;} else {extended_euclid_returning_gcd(b, a%b);
	T x1 = euclid_y; T y1 = euclid_x - (a / b) * euclid_y ; euclid_x = x1 ; euclid_y = y1 ;}}
template < class T > inline T absolute(T a ) { if(a>0) return a; else return -a;}
template < class T > inline T reverse_num( T a ){ T x=0,tmp = a; while(tmp) { x=10*x+ tmp % 10; tmp/=10;} return x;}
template <class T > inline T big_mod(T base, T power){ T res=1; while (power) { if(power&1){ res*=base; power--;} base =base *base; power/=2;} return res;}


char S [MX];             // input String
int sa[MX];             // indices of the sorted suffices
int pos[MX];            // Rank of the previous comparison
int tmp[MX];            // Counting Different Element
int gap;                // length of the comparable string
int N;

int kl[MX];
int sufCmp ( int i , int j)
{
      if( pos[i] != pos[j]){              // if previous compare is false
            return pos[i] < pos[j];
      }else {
            i += gap;                     //Comparing with the second base element
            j += gap;                     // (i > j ) denotes who finishes first or whose length is small
      }return (i < N && j < N ) ? (pos[i] < pos[j]) : (i > j);
      
}


void buildSA()
{
      N = strlen(S);
      
      /* This is a loop that initializes sa[] and pos[].
       For sa[] we assume the order the suffixes have
       in the given string. For pos[] we set the lexicographic
       rank of each 1-gram using the characters themselves.
       That makes sense, right? */
      REP(i, N) sa[i] = i, pos[i] = S[i];
      
      /* Gap is the length of the m-gram in each step, divided by 2.
       We start with 2-grams, so gap is 1 initially. It then increases
       to 2, 4, 8 and so on. */
      for (gap = 1;; gap *= 2)
      {
            /* We sort by (gap*2)-grams: */
            sort(sa, sa + N, sufCmp);
            
            /* We compute the lexicographic rank of each m-gram
             that we have sorted above. Notice how the rank is computed
             by comparing each n-gram at position i with its
             neighbor at i+1. If they are identical, the comparison
             yields 0, so the rank does not increase. Otherwise the
             comparison yields 1, so the rank increases by 1. */
            REP(i, N - 1) tmp[i + 1] = tmp[i] + sufCmp(sa[i], sa[i + 1]);
            
            /* tmp contains the rank by position. Now we map this
             into pos, so that in the next step we can look it
             up per m-gram, rather than by position. */
            REP(i, N) pos[sa[i]] = tmp[i];
            
            /* If the largest lexicographic name generated is
             n-1, we are finished, because this means all
             m-grams must have been different. */
            if (tmp[N - 1] == N - 1) break;
      }
}
int lcp[MX];

void buildLCP()
{
      for (int i = 0, k = 0; i < N; ++i) if (pos[i] != N - 1)
      {
            for (int j = sa[pos[i] + 1]; S[i + k] == S[j + k];)
                  ++k;
            lcp[pos[i]] = k;
            if (k)--k;
      }
}



int main()
{
	
      ll T , sum ;
      scanf("%lld\n",&T);
      while (T --) {
            while( 1){
                  gets(S);
                  if(strcmp(S , "")) break;
            }
            buildSA();
            
            buildLCP();
            ll best = -1 , maxi = -1 , bstcounter = -1 ;
 
            for(int i = 0 ; i < N ;  ){
                  if(lcp[i] > maxi ){
                        maxi = lcp[i];
                        int val = lcp[i] , j ,cnt = 0;
                        for( j = i ;j < N ; j ++ ){
                              if(lcp[j] != val) break;
                              cnt ++;
                              
                        }
                        bstcounter = cnt;
                        i = j;
                        best = i;
                  }else i ++;
            }
            if( maxi ){
                  for(int i = 0 ; i < maxi ; i ++){
                        printf("%c",S[sa[best] + i]);
                  }
                  printf(" %lld\n",bstcounter + 1);
            }else {
                  printf("No repetitions found!\n");
            }
           
            ms(tmp, 0);
            ms(lcp, 0);
            ms(sa, 0);
            ms(pos, 0);
            ms(kl, 0);
            
      }
	
	return 0;
}
