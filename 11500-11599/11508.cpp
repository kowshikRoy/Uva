#include <bits/stdc++.h>
using namespace std;

string str;
int main()
{
    while(getline(cin,str)) {
        int a;
        istringstream iss(str);
        int iM = -1;
        int cnt = 0;
        multiset<int> Pos,v;
        while(iss >> a) {
            iM = max(iM, a);
            cnt ++;
            v.insert(a);
        }
        if(iM < cnt ) {
            for(int i = 0; i < cnt; i ++ ) Pos.insert(i);
            map<int,int> ar;
            for(int i = 0; i < cnt ; i ++ ) {
                if(v.count(i)) {
                    v.erase(v.find(i));
                    Pos.erase(i);
                    ar[i] = i;
                }
            }
            for(auto a : v ) {
                ar[*Pos.begin()] = a ;
                Pos.erase(Pos.begin());
            }
            int f = 0;
            for(auto a : ar) {
                if(f) printf(" ");
                printf("%d",a.second);
                f = 1;
            }
            printf("\n");
            
        }
        else {
            printf("Message hacked by the Martians!!!\n");
        }
        
    }
}