#include <bits/stdc++.h>
using namespace std;

const int N = 1005;
string command[N];
int value[N];

struct PT {
    double x,y;
    PT(){}
    PT(double x,double y):x(x),y(y){}
    PT operator + (const PT &p) const {return PT(x+p.x,y+p.y);}
    PT operator - (const PT &p) const {return PT(x-p.x,y-p.y);}
    PT operator * (double c) const { return PT(x*c, y *c);}
    PT operator / (double c) const { return PT(x/c, y /c);}
    double mag(){return sqrt(x*x+y*y);}
};
PT RotCCW(PT p,double ang) {return PT(p.x*cos(ang) - p.y*sin(ang) , p.x*sin(ang) + p.y * cos(ang));}
#define PI 0.01745329251994329914588889
#define EPS 1e-2
int n;
PT F;

PT calc(int par= -1)
{
    PT pivot = PT(0,0) , dir = PT(1,0);
    for(int i = 0; i < n ; i ++ ) {
        if(i==par) {
            F= dir;
            continue;
        }
        string cmd = command[i];
        double d = value[i];
        if(cmd.compare("fd")== 0) pivot = pivot + dir * d;
        if(cmd.compare("bk")== 0) pivot = pivot - dir * d;
        if(cmd.compare("lt")== 0)  d = PI * d , dir =  RotCCW(dir,d);
        if(cmd.compare("rt")== 0)  d = PI * d , dir = RotCCW(dir,  - d) ;
    }
    return pivot;
}


int main()
{
    int t,  id ;
    cin >> t;
    while(t-- ) {
        cin>> n;
        id = 0;
        for(int i = 0; i < n ; i ++ ) {
            string cmd,t;
            cin >> cmd >> t;
            if(t.compare("?") == 0)  {
                id = i;
                command[i] = cmd;
            }
            else {
                command[i] = cmd;
                value[i] = atoi(t.c_str());
            }
        }
        if(command[id].compare("fd") == 0 || command[id].compare("bk") == 0) {
            PT f = calc(id);
            int sig = (command[id].compare("fd") == 0 ? -1 : 1);
            printf("%.0lf\n", f.x / F.x *  sig + EPS );
        }
        else {
            for(int ang = 0; ang < 360; ang ++ ) {
                value[id] = ang;
                PT r = calc();
                if(fabs(r.mag()) < EPS) {
                    printf("%d\n", ang);
                    break;
                }
            }
        }
        
    }
}

