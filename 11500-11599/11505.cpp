#include <bits/stdc++.h>
using namespace std;
struct PT {
    double x,y;
    PT(double x,double y):x(x),y(y){}
    PT operator + (const PT &p) const {return PT(x+p.x,y+p.y);}
    PT operator - (const PT &p) const {return PT(x-p.x,y-p.y);}
    PT operator * (double c) const { return PT(x*c, y *c);}
    PT operator / (double c) const { return PT(x/c, y /c);}
    double mag(){return sqrt(x*x+y*y);}
};

PT RotCCW(PT p,double ang) {return PT(p.x*cos(ang) - p.y*sin(ang) , p.x*sin(ang) + p.y * cos(ang));}
#define PI 0.01745329251994329914588889

int main()
{
    int T;
    cin>>T;
    while(T--) {
        int n;
        cin >> n;
        PT pivot = PT(0,0) , dir = PT(1,0);
        double d;
        char cmd[11];
        for(int i = 0;  i < n ; i ++ ) {
            scanf("%s %lf",cmd, &d);
            if(strcmp(cmd,"fd")== 0) pivot = pivot + dir * d;
            if(strcmp(cmd,"bk")== 0) pivot = pivot - dir * d;
            if(strcmp(cmd,"lt")== 0)  d = PI * d , dir =  RotCCW(dir,d);
            if(strcmp(cmd,"rt")== 0)  d = PI * d , dir = RotCCW(dir,  - d) ;
        }
        printf("%d\n",(int) (pivot.mag() + .5) );
    }
}

