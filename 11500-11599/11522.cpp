#include <bits/stdc++.h>
using namespace std;
bool flag[101];
const double EPS = 1e-8;
bool dfs(int sum,int p,double fr)
{

    if(fr> 1 + EPS || sum < 0) return 0;
    if(sum==0){
        if(fabs(fr-1) < EPS) return 1;
        return 0;
    }
    if(p>sum) return 0;
    return dfs(sum-p,p+1,fr + 1./p) || dfs(sum,p+1,fr);
}
int main()
{
    memset(flag,1,sizeof flag);
    for(int i= 2; i <= 77; i ++ ) {
        if(dfs(i,2,0) == 0) {
            flag[i] = 0;
        }
    }
    int t,a,b;
    scanf("%d",&t);
    while(t--) {
        scanf("%d %d",&a,&b);
        if(a>b)swap(a,b);
        int cnt = 0;
        while(a<=77 && a<= b) {
            if(flag[a]) cnt++;
            a++;
        }
        cnt += b - a + 1;
        printf("%d\n",cnt);
    }
}