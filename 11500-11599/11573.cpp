#include <bits/stdc++.h>
using namespace std;

int dir[][2] = {{-1,0},{-1,1},{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1}};
char grid[1005][1005];

int r,c,sx,sy,dx,dy;
int d[1000006];
int val(int x,int y) {return x*c+y;}
bool ok(int x,int y) {
    if(x>=0&&x<r && y>=0 && y < c) return 1;
    return 0;
}
void dij()
{
    memset(d,63,sizeof d);
    deque<int>q;
    q.push_back(val(sx,sy));
    d[val(sx,sy)] = 0;
    
    while(!q.empty()){
        int u = q.front(); q.pop_front();
        if(u == val(dx,dy)) break;
        int k = grid[u/c] [u%c] -'0';
        int xp = u/c + dir[k][0], yp = u%c + dir[k][1];
        if(ok(xp,yp) && d[val(xp,yp)] > d[u]) {
            d[val(xp,yp)] = d[u];
            q.push_front(val(xp,yp));
        }
        for(int i= 0; i < 8; i ++ ) {
            int xp = u/c + dir[i][0], yp = u%c + dir[i][1];
            if(ok(xp,yp) && d[val(xp,yp)] > 1 + d[u]) {
                d[val(xp,yp)] = 1 + d[u];
                q.push_back(val(xp,yp));
            }
        }
    }
    
}

int main()
{
    while(scanf("%d %d",&r,&c) == 2) {
        for(int i = 0; i < r; i ++ )scanf("%s",grid[i]);
        int q;
        scanf("%d",&q);
        while(q--) {
            scanf("%d %d %d %d",&sx,&sy,&dx,&dy);
            sx--,sy--,dx--,dy--;
            dij();
            printf("%d\n" , d[val(dx,dy)]);
        }
    }
}