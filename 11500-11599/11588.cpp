//
//  main.c
//  11588 - Image Coding
//
//  Created by MAC on 12/28/13.
//  Copyright (c) 2013 MAC. All rights reserved.
//

#include <stdio.h>
#include <ctype.h>
#include <string.h>

int ara[26];
char str[25],ch;
int main(int argc, const char * argv[])
{

    int test,row,col,n,m,cases=0,i,j,max,ans;
    scanf("%d",&test);
    
    
    while (test--) {
        scanf("%d %d %d %d\n",&row,&col,&m,&n);
        for (i=0; i<row; i++) {
            for (j=0; j<col;) {
                scanf("%c",&ch);
                if (isalpha(ch)) {
                    ara[ch-'A']++;
                    j++;
                }
                
            }
            getchar();
        }
        max=0;
        for (i=0; i<26; i++) {
            if (ara[i]>max) {
                max = ara[i];
            }
        
        }
        ans=0;
        
        for (i=0; i<26; i++) {
            if (ara[i]==max) {
                ans += m*ara[i];
            }
            else ans += n*ara[i];
        }
        
        
        printf("Case %d: %d\n",++cases,ans);
        memset(ara,0,sizeof(ara));
        
    }
    return 0;
}

