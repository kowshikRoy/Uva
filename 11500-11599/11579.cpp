//
//  main.cpp
//  11579 - Triangle Trouble
//
//  Created by Repon Macbook on 12/23/14.
//  Copyright (c) 2014 Repon Macbook. All rights reserved.
//


#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<queue>
#include<stack>
#include<cstdlib>
#include<algorithm>
#include<set>
#include<iterator>
#include<cassert>
#include <sstream>
#include <climits>
#include <list>
#include <string>
#include <map>
using namespace std;

/*------- Constants---- */
#define MX 31650
#define ll long long
#define ull unsigned long long
#define mod 1000000007
#define MEMSET_INF 63
#define MEM_VAL 1061109567
#define FOR(i,n) for( int i=0 ; i < n ; i++ )
#define mp(i,j) make_pair(i,j)
#define lop(i,a,b) for( int i = (a) ; i < (b) ; i++)
#define pb(a) push_back((a))
#define gc getchar_unlocked
#define EPS 1e-9
#define PI acos(-1)

/* -------Global Variables ------ */
ll euclid_x,euclid_y,d;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;

/*------ template functions ------ */
template < class T > inline T gcd(T a , T b ) { if(b==0) return a; else return gcd(b, a%b);}
template < class T > inline T lcm(T a , T b ) { return  a*b / gcd(a, b);}
template < class T > inline void extended_euclid_returning_gcd(T a,T b){ if(b==0){d = a;euclid_x=1;euclid_y=0;} else {extended_euclid_returning_gcd(b, a%b);
	T x1 = euclid_y; T y1 = euclid_x - (a / b) * euclid_y ; euclid_x = x1 ; euclid_y = y1 ;}}
template < class T > inline T absolute(T a ) { if(a>0) return a; else return -a;}
template < class T > inline T reverse_num( T a ){ T x=0,tmp = a; while(tmp) { x=10*x+ tmp % 10; tmp/=10;} return x;}
template <class T > inline T big_mod(T base, T power){ T res=1; while (power) { if(power&1){ res*=base; power--;} base =base *base; power/=2;} return res;}
template < class T > inline T mul_inv( T a , T b){T b0 = b, t, q; T x0 = 0, x1 = 1; if (b == 1) return 1;while (a > 1) { q = a / b;
        t = b, b = a % b, a = t; t = x0, x0 = x1 - q * x0, x1 = t; } if (x1 < 0) x1 += b0; return x1;
}



vector<double> edge;

double Triarea(double a, double b , double c)
{
        double s = ( a  + b + c) /2;
        return sqrt(s)*sqrt(s-a) * sqrt(s-b) *sqrt(s-c);
}
int main()
{
        
        
        int T , N ;
        cin >> T;
        double a;
        while (T -- ) {
                scanf("%d",&N);
                for(int i = 0; i < N ;i ++){
                        scanf("%lf",&a);
                        edge.push_back(a);
                }
                sort(edge.begin() , edge.end());
                
                double maxi = 0.0;
                for(int i = N - 1 ; i >=2 ; i --){
                        double p = edge[i];
                        double k = edge[i-1];
                        double l = edge[i-2];
                        
                        if( p < k + l ){
                                double t = Triarea(p,k,l);
                                if( t > maxi) maxi  = t;
                        }
                        
                }
                printf("%.2lf\n",maxi);
                edge.clear();
        }
        return 0;
        
}
