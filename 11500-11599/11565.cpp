//
//  main.c
//  11565 - Simple Equations
//
//  Created by MAC on 11/23/13.
//  Copyright (c) 2013 MAC. All rights reserved.
//

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(int argc, const char * argv[])
{

    int a,b,c,n,x,y,z,limx,limy,sec,flag;
    scanf("%d",&n);
    
    while (n--) {
        scanf("%d %d %d",&a,&b,&c);
        limx = sqrt(c/3.);
        flag = 1;
        for (x=-limx; x<=limx; x++) {
            if (x && b%x==0) {
                sec = abs(b/x);
                limy = sqrt(sec);
                for (y=x+1; y<=limy; y++) {
                    if (y==x) {
                        continue;
                    }
                    if (y && sec % y==0) {
                        z = abs(sec/y);
                        if (z==y) {
                            continue;
                        }
                        if (x+y+z==a && x*x + y*y + z*z==c) {
                            printf("%d %d %d\n",x,y,z);
                            flag =0 ;
                            break;
                        }
                    }
                }
                if (flag==0) {
                    break;
                }
            }
        }
        if (flag) {
            printf("No solution.\n");
        }
    }
    return 0;
}

