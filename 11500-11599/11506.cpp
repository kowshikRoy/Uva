//
//  main.cpp
//  11506 - Angry Programmer
//
//  Created by Repon Macbook on 4/11/15.
//  Copyright (c) 2015 Repon Macbook. All rights reserved.
//

#include <bits/stdc++.h>
using namespace std;

/*------- Constants---- */
#define LMT                     100
#define ll                      long long
#define ull                     unsigned long long
#define mod                     1000000007
#define MEMSET_INF              63
#define MEM_VAL                 1061109567
#define FOR(i,n)                for( int i=0 ; i < n ; i++ )
#define mp(i,j)                 make_pair(i,j)
#define lop(i,a,b)              for( int i = (a) ; i < (b) ; i++)
#define pb(a)                   push_back((a))
#define gc                      getchar_unlocked
#define PI                      acos(-1.0)
#define inf                     1<<29
#define lc                      ((n)<<1)
#define rc                      ((n)<<1 |1)
#define msg(x)			  cout<<x<<endl;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;
/*------ template functions ------ */
inline void sc(int &x)
{
	register int c = gc();
	x = 0;
	int neg = 0;
	for(;((c<48 | c>57) && c != '-');c = gc());
	if(c=='-') {neg=1;c=gc();}
	for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
	if(neg) x=-x;
}

template <class T> inline T bigmod(T p,T e,T M){
	ll ret = 1;
	for(; e > 0; e >>= 1){
		if(e & 1) ret = (ret * p) % M;
		p = (p * p) % M;
	} return (T)ret;
}
template <class T> inline T gcd(T a,T b){if(b==0)return a;return gcd(b,a%b);}
template <class T> inline T modinverse(T a,T M){return bigmod(a,M-2,M);}

/*************************** END OF TEMPLATE ****************************/

const int  MAXN = 150;
const int MAXM = MAXN * MAXN  ;
#define LL int

struct edge
{
	int u,v;
	LL cap,flow ;
	int next;
};

int eh[MAXN],tot;
edge et[MAXM];

void add(int u,int v,LL cap,LL f)
{
	edge E={u,v,cap,f,eh[u]};
	et[tot]=E,eh[u]=tot++;
}
void addEdge(int u,int v,LL cap)
{
	add(u , v, cap,0),add(v, u, 0 , 0);
}


int cur[MAXN],cnt[MAXN],dis[MAXN],pre[MAXN];
LL low[MAXN];

void rebfs(int s,int t,int n)
{
	for(int i=0;i<=n;i++)
	{
		cnt[i]=0;
		dis[i]=n;
	}
	queue<int>q;
	dis[t]=0;
	cnt[0]=1;
	q.push(t);
	while(!q.empty())
	{
		int u=q.front();
		q.pop();
		for(int now = eh[u]; now!=-1 ; now=et[now].next)
		{
			
			if(et[now^1].cap == 0 || dis[et[now].v] <n)continue;
			dis[et[now].v]=dis[u]+1;
			cnt[dis[et[now].v]]++;
			q.push(et[now].v);
		}
	}
}


LL isap(int s,int t,int n)
{
	rebfs(s,t,n);
	int u,v,now,i;
	for(i=0;i<=n;i++)
	{
		//    dis[i]=0;
		low[i]=0;
		//     cnt[i]=0;
	}
	for(u=0;u<=n;u++)
		cur[u]=eh[u];
	LL flow=0;
	u=s;
	low[s]=inf;
	cnt[0]=n;
	while(dis[s]<n)
	{
		for( now=cur[u]; now!=-1; now=et[now].next){
			
			if( et[now].cap - et[now].flow > 0 && dis[u] == dis[v=et[now].v] +1 ) break;
			
		}
		if(now!=-1)
		{
			cur[u] = now;
			pre[v] = now;
			low[v] = et[now].cap - et[now].flow ;
			low[v] = low[v]<low[u] ? low[v]:low[u];
			
			u=v;
			
			if(u==t)
			{
				for( ; u!=s  ; u=et[pre[u]].u)
				{
					et[pre[u]].flow += low[t];
					et[pre[u]^1].flow -=low[t];
				}
				flow += low[t];
				low[s]=inf;
			}
		}
		else
		{
			if(--cnt[dis[u]] == 0 )break;
			
			dis[u] = n;
			cur[u] = eh[u];
			for( now=eh[u] ; now!=-1 ; now=et[now].next )
				
				if(et[now].cap - et[now].flow > 0 && dis[u] > dis[et[now].v]+1)
					
					dis[u]=dis[et[now].v]+1;
			
			
			cnt[dis[u]]++;
			
			if( u != s) u = et[pre[u]].u;
		}
	}
	return flow;
}

void init()
{
	tot=0;
	memset(eh,-1,sizeof(eh));
}


int rg[LMT];

int main()
{
	
	
	int  M , W , a ,b , c , id ;
	
	while (1) {
		sc(M) ; sc(W);
		if(M == 0 && W ==0 ) break;
		
		init();
		
		for(int i = 2; i < M ; i ++) {
			sc(id) ; sc(rg[i]);
			addEdge(id , id + M , rg[i]);
		}
		
		while (W -- ) {
			sc(a) ;sc(b) ; sc(c)  ;
			int x = min(a,b);
			int y = max(a,b);
			
			if(x == 1) {
				addEdge(x, y, c);
			}
			else {
				addEdge(a + M , b, c);
				addEdge(b + M , a, c);
			}
		}
		addEdge(M , M + M + 1, inf );
		addEdge(M + M , M + M + 1, inf );
		
		
		int ans = isap(1 , 2 * M + 1 , 2 * M + 2);
		printf("%d\n",ans );
	}
	return 0;
}

/*
 
 4 4 
 3 5 
 2 2 
 1 2 3 
 1 3 3
 2 4 1
 3 4 3 
 4 4
 3 2
 2 2
 1 2 3
 1 3 3
 2 4 1
 3 4 3 
 0 0
 
 */

