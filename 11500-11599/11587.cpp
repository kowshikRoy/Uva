#include <bits/stdc++.h>
using namespace std;

char P[105];
int dp[105];
vector<int> mov;
char game[1<<20][101];
int calc(int n)
{
	if(n==0) return 0;
	if(dp[n] !=-1) return dp[n];
	for(int i = 0; i < mov.size(); i ++ ) {
		if(n >= mov[i] && calc(n-mov[i]) == 0) return dp[n] = 1; 
	}
	return dp[n] = 0;
}
int main()
{
	//freopen("in.txt","r",stdin);
	for(int i = 1; i < 1 << 20; i ++ ) {
		mov.clear();
		for(int j = 0; j < 20; j ++ ) {
			if(i&1<<j) {
				mov.push_back(j+1);
			}
		}
		memset(dp,-1,sizeof dp);
		for(int j = 100; j >= 1; j -- ) {
			int v = (dp[j] == -1 ? calc(j) : dp[j]);
			game[i][j-1] = (v ? 'W' : 'L');
		}
	}
	int n;
	scanf("%d",&n);
	int cs = 0;
	for(int i = 0; i < n ; i ++ ) {
		scanf("%s",P);
		for(int i = 1; i < 1 << 20; i ++ ) {
			bool flag = 1;
			for(int j = 0; P[j] ; j ++ ) {
				if(game[i][j] != P[j]) {
					flag= 0;
				}
			}
			if(flag) {
				printf("Case %d:",++cs);
				for(int j = 0; j < 20; j ++ ) {
					if(i&1<<j) {
						printf(" %d",j+1);
					}
				}
				printf("\n");
				break;
			}
		}
	}
}