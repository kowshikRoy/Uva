#include <bits/stdc++.h>
using namespace std;
#define MP make_pair
int fib[10000];
int bigmod(unsigned long long a, unsigned long long b, int mod)
{
    if(b==0)return 1;
    if(b%2) {
        return ((a % mod ) * bigmod(a,b-1,mod) )% mod;
    }
    int p = bigmod(a, b/2 , mod);
    return (p * p) % mod;
}
int main()
{
    int t;
    unsigned long long a,b,n;
    scanf("%d",&t);
    while(t--){
        scanf("%llu %llu %llu",&a,&b,&n);
        map<pair<int,int>, int > Map;
        
        int f0=0,f1=1, i = 2;
        Map[MP(f0,f1)]=0;
        fib[0] = 0;
        fib[1] = 1;
        for( i = 2; ; i ++) {
            int now = f0 + f1;
            f0 = f1;
            f1 = now;
            f0%= n;
            f1%= n;
            if(Map.count(MP(f0,f1))) break;
            Map[MP(f0,f1)] = i-1;
            fib[i] = f1;
        }
        int period = i-1 - Map[MP(f0,f1)];
       // cout << period << endl;
        int mod = bigmod(a,b,period);
        //cout << mod << endl;
        int pos = ((mod - Map[MP(f0,f1)]) % period + period ) % period;
        //cout << pos << endl;
        printf("%d\n",fib[pos]);
        Map.clear();
    }
}
