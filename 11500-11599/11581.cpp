//
//  main.cpp
//  11581 - Grid Successors
//
//  Created by MAC on 2/11/14.
//  Copyright (c) 2014 MAC. All rights reserved.
//

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<map>
#include<queue>
#include<stack>
#include<cstdlib>
#include<algorithm>
#include<set>
#include<iterator>
#include<cassert>

using namespace std;

/*------- Constants---- */
#define MX 1030
#define ll long long
#define ull unsigned long long

/* -------Global Variables ------ */
ll x,y;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))


/*------ template functions ------ */
template < class T > inline T gcd(T a , T b ) { if(b==0) return a; else return gcd(b, a%b);}
template < class T > inline T lcm(T a , T b ) { return  a*b / gcd(a, b);}
template < class T > inline T extended_euclid_returning_gcd(T a,T b){ T t,d; if(b==0){d = a;x=1;y=0;} else {extended_euclid_returning_gcd(b, a%b); t=x; x=y;y = t-(a*y/b);}}
template < class T > inline T absolute(T a ) { if(a>0) return a; else return -a;}
template < class T > inline T reverse_num( T a ){ T x=0,tmp = a; while(tmp) { x=10*x+ tmp % 10; tmp/=10;} return x;} 
ll power ( ll a, ll b){ ll x=1; while(b--) x*=a; return x;}

int ara[5][5],sec[5][5];

int main()
{
    int test,flag,ans;
    scanf("%d",&test);
    
    while (test--) {
        flag=0;
        ans=-1;
        for(int i=1;i<4;i++) {
            for (int j=1; j<4; ) {
                char ch=getchar();
                if(ch<='9' && ch>='0') {
                    ara[i][j]=ch-'0';
                    if(ch!='0') {flag=1;}
                    j++;
                }
            }
        }
        while (flag) {
            flag=0;
            for (int i=1; i<4; i++) {
                for (int j=1; j<4; j++) {
                    sec[i][j]=(ara[i-1][j]+ara[i+1][j]+ara[i][j+1]+ara[i][j-1])%2;
                }
            }
            for (int i=1; i<4; i++) {
                for (int j=1; j<4; j++) {
                    ara[i][j]=sec[i][j];
                    if(ara[i][j]) flag=1;
                }
            }
            ans++;
        }
        printf("%d\n", ans);
    }
    return 0;
}