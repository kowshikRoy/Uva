//
//  main.cpp
//  11536 Smallest Sub-Array
//
//  Created by Repon Macbook on 4/28/15.
//  Copyright (c) 2015 Repon Macbook. All rights reserved.
//
#include <bits/stdc++.h>
using namespace std;

/*------- Constants---- */
#define LMT				1000006
#define ll				long long
#define ull				unsigned long long
#define mod				1000000007
#define MEMSET_INF		63
#define MEM_VAL			1061109567
#define FOR(i,n)			for( int i=0 ; i < n ; i++ )
#define mp(i,j)			make_pair(i,j)
#define lop(i,a,b)		for( int i = (a) ; i < (b) ; i++)
#define pb(a)			push_back((a))
#define gc				getchar_unlocked
#define PI				acos(-1.0)
#define inf				1<<30
#define lc				((n)<<1)
#define rc				((n)<<1 |1)
#define msg(x)			cout<<x<<endl;

/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;
/*------ template functions ------ */
inline void sc(int &x)
{
	register int c = gc();
	x = 0;
	int neg = 0;
	for(;((c<48 | c>57) && c != '-');c = gc());
	if(c=='-') {neg=1;c=gc();}
	for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
	if(neg) x=-x;
}

template <class T> inline T bigmod(T p,T e,T M){
	ll ret = 1;
	for(; e > 0; e >>= 1){
		if(e & 1) ret = (ret * p) % M;
		p = (p * p) % M;
	} return (T)ret;
}
template <class T> inline T gcd(T a,T b){if(b==0)return a;return gcd(b,a%b);}
template <class T> inline T modinverse(T a,T M){return bigmod(a,M-2,M);}

/*************************** END OF TEMPLATE ****************************/


int F[LMT] ;
int MOD ;

int vis[105];
int main()
{
	
	
	int tc ,cs =0 , n , k ;
	sc(tc);
	while (tc -- ) {
		sc(n) ;
		sc(MOD);
		sc(k);
		
		F[0] = 1;
		F[1] = 2;
		F[2] = 3;
		for(int i = 3 ; i <n ; i ++ ){
			F[i] = (F[i-1] + F[i-2] + F[i-3]) % MOD + 1 ;
			
		}
		int L = 0 , R = 0;
		int minLen = inf ;
		int cnt = 0;
		ms(vis , 0);
		while( R  < n) {
			int i = R;
			if(F[i] <= k && F[i] > 0 ){
				if(vis[F[i]] == false ) {
					cnt ++;
				}
				vis[F[i]] ++;
			}
			if(cnt == k) {
				while(L <= R ){
					if(F[L] > k ) {
						++L;
						continue;
					}
					
					if(vis[F[L]] > 1 ){
						vis[F[L]] --;
						L ++;
					}
					else break;
					
				}
				if( minLen > R - L + 1) {
					minLen = R - L + 1;
				}
			}
			R ++;
		}
		
		printf("Case %d: ",++cs);
		if(minLen == inf) printf("sequence nai\n");
		else printf("%d\n" ,minLen);
		
	}
	return 0;
}

