#include <bits/stdc++.h>
using namespace std;
int b,t,CS = 1;
double f;
double p[12];
int s[12];
double dp[1<<10][105][105];
int vis[1<<10][105][105];
int fail[12];
double calc(int Mask, int T, int F )
{
    if(Mask+1 == (1<<b))return 0;
    if(T == 0 ) return 0;
    if(vis[Mask][T][F] == CS) return dp[Mask][T][F];
    
    int id = -1;
    double iM = -1e8;
    for(int i= 0;i < b; i++) {
        if(!(Mask & 1 << i)) {
            if(p[i] * s[i] > iM ) {
                iM = p[i] * s[i];
                id = i;
            }
        }
    }
    double res = p[id] * (s[id] + calc(Mask | 1 << id , T-1, F - fail[id]));
    double prv = p[id];
    p[id] *= f;
    fail[id] ++;
    res += (1- prv) * calc(Mask, T -1 , F + 1);
    p[id] = prv;
    fail[id] --;
                          
    vis[Mask][T][F]  = CS;
    return dp[Mask][T][F] = res ;
    
}
int main()
{
    while(scanf("%d %d %lf",&b,&t,&f)==3) {
        for(int i = 0; i < b; i ++ ) scanf("%lf %d",&p[i],&s[i]);
        printf("%.9lf\n", calc(0,t,0));
        CS ++;
    }
    
}
