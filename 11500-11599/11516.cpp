#include <bits/stdc++.h>
using namespace std;
const int N = 1e5+5;

int x[N];
int n,m;

bool can(int M)
{
    int prv = -1e8 , cnt = 0;
    for(int i= 0; i < m ;i ++ ) {
        if(x[i] - prv > M) {
            cnt ++;
            prv = x[i];
        }
    }
    return cnt <= n;
}
int main()
{
    int t;
    scanf("%d",&t);
    while(t--) {
        scanf("%d %d",&n,&m);
        for(int i = 0; i < m ;i ++ ) scanf("%d",&x[i]);
        sort(x,x+m);
        int low = 0, high = 1e6+6 , mid , ans = 0;
        while(low <= high) {
            mid = (low + high) / 2;
            if(can(mid)) {
                ans = mid;
                high = mid-1;
            }else low = mid+1;
        }
        printf("%.1lf\n" , ans * 1./ 2);
    }
}