#include <bits/stdc++.h>
using namespace std;
const int N = 205;
map<string,int> Name;
int cnt = 0;
int val(string s)
{
    if(Name.count(s)) return Name[s];
    Name[s] = ++cnt;
    return cnt;
}
int ar[N];
int dp[N][N];
int n;
vector<int>v;
int F(int i,int j)
{
    if(i>j) return 0;
    
    if(i==j) {
        return 1;
    }
    if(dp[i][j] !=-1) return dp[i][j];
    int res = 1e5;
    res = F(i+1,j) + 1;
    for(int k = i+1; k <= j ; k ++ ) {
        if(v[k] == v[i]) {
            res = min(res , F(i+1,k) + F(k+1,j));
        }
    
    }
    return dp[i][j] =res;
}
int main()
{
    
    int t,cs = 0;
    cin>>t;
    while(t--) {
        cin >> n;
        string s;
        cnt=0;
        for(int i = 0; i < n ;i ++ ) {
            cin >> s;
            if(islower(s[0])) {
                ar[i] = val(s);
            }
            else {
                ar[i] = 0;
            }
        }
        int prv = -1;
        v.clear();
        for(int i = 0; i < n ;i ++ ) {
            if(ar[i] != prv) {
                v.push_back(ar[i]);
                prv = ar[i];
            }
        }
        memset(dp,-1,sizeof dp);
        vector<int> R;
        R.push_back(-1);
        for(int i = 0; i < v.size();i ++ ) if(v[i] == 0) R.push_back(i);
        R.push_back(v.size());
        int ans = 0;
        for(int i = 0; i < R.size()-1; i ++ ) {
            ans += F(R[i]+1, R[i+1]-1);
        }
        printf("Case %d: %d\n",++cs, ans);
        Name.clear();
    }
}