//
//  main.cpp
//  11576 - Scrolling Sign
//
//  Created by Repon Macbook on 5/29/15.
//  Copyright (c) 2015 utpal roy. All rights reserved.
//

#include <bits/stdc++.h>
using namespace std;

/*------- Constants---- */
#define LMT				205
#define ll				long long
#define ull				unsigned long long
#define mod				1000000007
#define MEMSET_INF			63
#define MEM_VAL			1061109567
#define FOR(i,n)			for( int i=0 ; i < n ; i++ )
#define mp(i,j)			make_pair(i,j)
#define lop(i,a,b)			for( int i = (a) ; i < (b) ; i++)
#define pb(a)				push_back((a))
#define gc				getchar_unlocked
#define PI				acos(-1.0)
#define inf				1<<30
#define lc				((n)<<1)
#define rc				((n)<<1 |1)
#define debug(x)              cout<<#x<<" -> " << x << endl;
/*---- short Cuts ------- */
#define ms(ara_name,value) memset(ara_name,value,sizeof(ara_name))
typedef pair<int, int> ii;
typedef vector<int > vi ;
/*------ template functions ------ */
inline void sc(int &x)
{
	register int c = gc();
	x = 0;
	int neg = 0;
	for(;((c<48 | c>57) && c != '-');c = gc());
	if(c=='-') {neg=1;c=gc();}
	for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
	if(neg) x=-x;
}

template <class T> inline T bigmod(T p,T e,T M){
	ll ret = 1;
	for(; e > 0; e >>= 1){
		if(e & 1) ret = (ret * p) % M;
		p = (p * p) % M;
	} return (T)ret;
}
template <class T> inline T gcd(T a,T b){if(b==0)return a;return gcd(b,a%b);}
template <class T> inline T modinverse(T a,T M){return bigmod(a,M-2,M);}

/*************************** END OF TEMPLATE ****************************/


char s[LMT][LMT];
char T[LMT * LMT ] ;
int PF[LMT];


void compute(char P[] , int Psize)
{
      PF[0] = 0;
      for (int i = 1; i < Psize ; i ++ ) {

            int j = PF[i-1];
            while (j > 0 && P[i] != P[j] ) {
                  j = PF[j-1];
            }
            if(P[i] == P[j]) ++j;
            PF[i] = j;
      }
}

int main()
{
	
	
      int tc , n , m ;
      sc(tc);
      while (tc --) {
            sc(n);sc(m);
            for(int i = 0 ; i < m ; i ++) {
                  scanf("%s", s[i]);
            }
            
            int cnt = 0;
            
            for(int i =0 ; s[0][i] ; i ++) T[cnt++] = s[0][i];
           
            for(int i = 1; i < m ; i ++ ) {
                  
                  int len = n;
                  
                  compute(s[i] , n);
                  
                  int k = 0;
                  int alc = 0;
                  for(int L =0 ; L < cnt ; L ++) {
                        while( k > 0 && T[L] != s[i][k]) k = PF[k-1];
                        if(T[L] == s[i][k]) ++k;
                        
                        if(k == n) {
                              alc = n;
                              k = PF[k-1];
                        }
                  }
                  
                  alc = max(alc , k);
                  for(int L = alc; L < n;  L ++) {
                        T[cnt++] = s[i][L];
                  }
            }
            
            printf("%d\n" ,cnt);
      }
	
	return 0;
}
/*
2
3 2
CAT
TED
3 3
CAT
ATE
TEA
*/