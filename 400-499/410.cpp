/*
Solution : http://www.algorithmist.com/index.php/UVa_410
*/

#include<bits/stdc++.h>
using namespace std;

int n,c,cs=1;
double avg;
int a[12];
int main()
{
    while(scanf("%d %d",&c ,&n)==2){
        double sum = 0;
        memset(a,0,sizeof a);
        for(int i = 0;i < n ; i ++ ) {
            scanf("%d",&a[i]);
            sum += a[i];
        }
        avg = sum / c;
        sort(a,a+2*c);
        double p = 0;
        printf("Set #%d\n",cs++);
        for(int i = 0; i < c; i ++ ){
            printf("%2d:",i);
            if(a[i] > 0) printf(" %d",a[i]);
            if(a[2*c-i-1] > 0) printf(" %d",a[2*c-1-i]);
            p += fabs(a[i] + a[2*c-1-i] - avg);
            printf("\n");
        }
        printf("IMBALANCE = %.5lf\n\n",p);
        
    }
}