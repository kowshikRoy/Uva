#include<bits/stdc++.h>
using namespace std;
const int N = 1e6+1;
int d[N];
int p[N];
int main()
{
    for(int i = 1; i <N ; i ++ ) {
        for(int j = i; j < N; j +=i) {
            d[j]++;
        }
    }
    for(int i = 1;i < N; i ++ ) {
        if(d[i] >= d[p[i-1]]) {
            p[i] = i;
        }
        else p[i] = p[i-1];
    }
    int t;
    scanf("%d",&t);
    while(t--) {
        int n;
        scanf("%d",&n);
        printf("%d\n",p[n]);
    }
    
    return 0;
}