#include <bits/stdc++.h>
using namespace std;
const int N = 1<<10;
int dp[N];
int a[N*2];
int n;
bool flag = 0;
void calc(int sum,int cnt, int ind)
{
    if(cnt==n){
        if(sum % n ==0){
            flag = 1;
            printf("Yes\n");
            for(int i = 0; i < n;i ++) {
                if(i)printf(" ");
                printf("%d",dp[i]);
            }
            printf("\n");
        }
        return;
    }
    if(flag) return;
    if(ind >= 2*n-1) return;
    dp[cnt] = a[ind];
    calc(sum + a[ind], cnt+1, ind+1);
    calc(sum,cnt,ind+1);
    
}
int main()
{
    while(scanf("%d",&n) && n) {
        for(int i = 0; i < 2*n -1;i ++ ) {
            scanf("%d",&a[i]);
        }
        memset(dp,0,sizeof dp);
        flag = 0;
        calc(0,0,0);
        if(!flag) printf("No\n");
    }
    
}