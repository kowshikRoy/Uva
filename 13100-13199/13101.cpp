#include<bits/stdc++.h>
using namespace std;

const int N = 50005;
const int LG = 18;
int n;
int P[N][20], lev[N], ch = 0;
int chainNo[N], head[N], subsize[N], child[N], pos[N];
int Ar[N], tot = 0 , value[N];

vector<int> G[N];
int seg[4*N];

int gcd(int a,int b)
{
    if(b==0)return a;
    return gcd(b,a%b);
}
void build(int n,int b,int e)
{
    if(b==e) {
        seg[n] = Ar[b];
        return;
    }
    int mid = (b+e)/2;
    build(2*n,b,mid);
    build(2*n+1,mid+1,e);
    seg[n] = gcd(seg[2*n],seg[2*n+1]);
}

int query(int n,int b,int e,int i,int j)
{
    if(b>j||e<i)return 0;
    if(b>=i&&e<=j) return seg[n];
    int mid = (b+e)/2;
    int p1 = query(2*n,b,mid,i,j), p2 = query(2*n+1,mid+1,e,i,j);
    return gcd(p1,p2);
}
void update(int n,int b,int e,int pos,int v)
{
    if(b>pos || e < pos) return;
    if(b==e && b==pos) {
        seg[n] = v;
        return;
    }
    int mid = (b+e)/2;
    if(pos <= mid )update(2*n,b,mid,pos,v);
    else update(2*n+1,mid+1,e,pos,v);
    seg[n] = gcd(seg[2*n], seg[2*n+1]);
}
/********** MAIN HLD-PART **********************/
void dfs(int u,int p)
{
    subsize[u] = 1;
    for(int i = 0; i < G[u].size(); i ++ ) {
        int v = G[u][i];
        if(v == p ) continue;
        lev[v] = 1  + lev[u];
        P[v][0] = u; // Modify
        dfs(v,u);
        
        subsize[u] += subsize[v];
        if(child[u] == -1 || subsize[v] > subsize[child[u]] ) {
            child[u] = v;
        }
        
    }
}
void HLD(int u,int p)
{
    if(head[ch] == -1) head[ch] = u;
    Ar[ ++tot ] = value[u];
    pos[u] = tot;
    chainNo[u] = ch;
    
    if(child[u]  > 0 ) HLD(child[u], u );
    for(int i = 0; i < G[u].size(); i ++ ) {
        int v = G[u][i];
        if(v == p || v == child[u]) continue;
        ++ch;
        HLD(v,u);
    }
}

int LCA(int u,int v)
{
    if(lev[u] > lev[v]) swap(u,v);
    int d = lev[v] - lev[u];
    for(int i = LG - 1; i >= 0 ;  i -- ) {
        if( d  &1<<i){
            v = P[v][i];
        }
    }
    
    if(u==v) return u;
    for(int i = LG-1; i >= 0 ; i -- ) {
        if(P[u][i] != P[v][i]) {
            u = P[u][i];
            v = P[v][i];
        }
    }
    return P[v][0];
}

int queryUp(int from, int to) // Climb up to
{
    int ret = 0;
    while(chainNo[from ] != chainNo[to] ) {
        int h = head[chainNo[from]];
        ret = gcd(ret, query(1,1,tot, pos[h], pos[from])); // Modify
        from = P[h][0];
    }
    ret = gcd(ret, query(1,1,tot,pos[to], pos[from])) ; // Modify
    return ret;
}
void process()
{
    memset(P,-1,sizeof(P));
    memset(head,-1,sizeof(head));
    memset(child,-1,sizeof(child));
    
    tot = 0 , ch = 0;
    dfs(0,-1);
    HLD(0,-1);
    build(1,1,tot);
    for(int i = 1; i < LG; i ++ ) {
        for(int j = 0; j <  n; j ++ ) if(P[j][i-1] !=-1 ) P[j][i] = P[P[j][i-1]][i-1];
    }
}
/************************** END ************************/


int query(int a,int b)
{
    int l = LCA(a,b);
    int p1 = queryUp(a,l);
    int p2 = queryUp(b,l);
    return gcd(p1,p2);
}
int main()
{
    //freopen("/Users/MyMac/Desktop/in.txt","r",stdin);
    int a,b,c,d,t;
    while(scanf("%d",&n)==1) {
        for(int i=0;i<n;i++)scanf("%d",&value[i]);
        for(int i = 0; i < n-1; i ++) {
            scanf("%d %d",&a,&b);
            G[a].push_back(b);
            G[b].push_back(a);
        }
        process();
        int q;
        scanf("%d",&q);
        while(q--){
            scanf("%d %d %d",&t,&a,&b);
            if(t==1) {
                printf("%d\n",query(a,b));
            }
            else {
                int p = pos[a];
                update(1,1,tot,p,b);
            }
        }
        
        for(int i = 0; i < n; i ++) G[i].clear();
    }
    return 0;
}